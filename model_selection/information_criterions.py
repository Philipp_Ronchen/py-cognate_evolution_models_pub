# calculates the Akaike information criterion value of a given model
def calculate_aic(max_log_lh, num_parameters):
    return 2 * num_parameters - 2 * max_log_lh