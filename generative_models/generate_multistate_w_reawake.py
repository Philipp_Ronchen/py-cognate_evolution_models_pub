from generative_models import simulate_poisson_process as spp
from generative_models import reawake_cognate_tools as rct
from trees import tree_tools as tt
import copy


def generate_one_meaning_outcome(tree, change_rate, reawake_rate):
    nodes = tree[0]
    edges = tree[1]
    root = tree[2]
    parents = tree[5]
    if not edges:
        raise ValueError("tree needs to contain at least one edge")
    [edge_ordered_points, edge_points_types] = sample_edge_points(edges, change_rate, reawake_rate)
    print("edge ordered points")
    print(edge_ordered_points)
    print("edge points types")
    print(edge_points_types)
    to_node_ordered_ps = dict()
    to_node_from_root_dists = dict()
    to_node_p_values = dict()
    to_node_value_weights = dict()
    max_assigned_value = 0
    levels = tt.get_downward_levels(tree)
    for node in levels[1]:
        edge = (root, node)
        print("edge:")
        print(edge)
        [to_node_ordered_ps[node], to_node_from_root_dists[node], to_node_p_values[node], to_node_value_weights[node]] = \
            rct.initialise_point_path(edge)
        print([to_node_ordered_ps[node], to_node_from_root_dists[node], to_node_p_values[node], to_node_value_weights[node]])
        for new_point in edge_ordered_points[edge]:
            print("point:")
            print(new_point)
            point_type = edge_points_types[edge][new_point]
            print("point type")
            print(point_type)
            max_assigned_value = rct.add_point_and_value_to_point_path(tree, new_point, point_type,
                                                                       to_node_ordered_ps[node],
                                                                       to_node_from_root_dists[node],
                                                                       to_node_p_values[node],
                                                                       to_node_value_weights[node], max_assigned_value)
    for level in levels[2:]:
        print("level:")
        print(level)
        for node in level:
            parent = parents[node]
            edge = (parent, node)
            print("edge:")
            print(edge)
            to_node_ordered_ps[node] = copy.deepcopy(to_node_ordered_ps[parent])
            to_node_from_root_dists[node] = copy.deepcopy(to_node_from_root_dists[parent])
            to_node_p_values[node] = copy.deepcopy(to_node_p_values[parent])
            to_node_value_weights[node] = copy.deepcopy(to_node_value_weights[parent])
            for new_point in edge_ordered_points[edge]:
                print("new point")
                print(new_point)
                point_type = edge_points_types[edge][new_point]
                print("point type")
                print(point_type)
                max_assigned_value = rct.add_point_and_value_to_point_path(tree, new_point, point_type,
                                                                           to_node_ordered_ps[node],
                                                                           to_node_from_root_dists[node],
                                                                           to_node_p_values[node],
                                                                           to_node_value_weights[node],
                                                                           max_assigned_value)
    cog_data = dict()
    cog_data[root] = 0
    for node in nodes.difference({root}):
        parent = parents[node]
        edge = (parent, node)
        node_point = [point for point in edge_ordered_points[edge] if edge_points_types[edge][point] == "node"][0]
        node_value = to_node_p_values[node][node_point]
        cog_data[node] = node_value
    return cog_data


def sample_edge_points(edges, change_rate, reawake_rate):
    edge_ordered_points = dict()
    edge_points_types = dict()
    for edge in edges:
        edge_change_point_positions = spp.generate_points_line(change_rate, edges[edge])
        edge_reawake_point_positions = spp.generate_points_line(reawake_rate, edges[edge])
        edge_unordered_points = list()
        edge_points_types[edge] = dict()
        for position in edge_change_point_positions:
            new_point = (edge, position)
            edge_unordered_points.append(new_point)
            edge_points_types[edge][new_point] = "change"
        for position in edge_reawake_point_positions:
            new_point = (edge, position)
            edge_unordered_points.append(new_point)
            edge_points_types[edge][new_point] = "reawake"
        # append children node as point
        new_point = (edge, edges[edge])
        edge_unordered_points.append(new_point)
        edge_points_types[edge][new_point] = "node"

        edge_ordered_points[edge] = sorted(edge_unordered_points)
    return [edge_ordered_points, edge_points_types]