from generative_models import loan_tools
from generative_models import generate_multistate as gm


def generate_outcome(tree, meanings, change_rates, loan_intensities):
    cog_data = dict()
    for meaning in meanings:
        change_rate = change_rates[meaning]
        loan_intensity = loan_intensities[meaning]
        meaning_outcome = generate_one_meaning_outcome(tree, change_rate, loan_intensity)
        cog_data[meaning] = meaning_outcome
    return cog_data


def generate_one_meaning_outcome(tree, change_rate, loan_intensity):
    loan_list = loan_tools.generate_loans(tree, loan_intensity)
    outcome = generate_one_meaning_given_loans(tree, change_rate, loan_list)
    return outcome


def generate_one_meaning_given_loans(tree, change_rate, loan_list):
    nodes = tree[0]
    loan_tree = loan_tools.change_topology_by_loan_list(tree, loan_list)
    loan_tree_outcome = gm.generate_one_meaning_outcome(loan_tree, change_rate)
    outcome = {node:loan_tree_outcome[node] for node in nodes}
    return outcome






