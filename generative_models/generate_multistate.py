import random as rd
import math
from trees import tree_tools


# generates cognate date for a given tree in accordance to the multistate model
# meanings is the set of meanings
# change_rates is a dictionary assigning to every meaning a change rate
def generate_outcome(tree, meanings, change_rates):
    cog_data = dict()
    for m in meanings:
        change_rate = change_rates[m]
        cog_data[m] = generate_one_meaning_outcome(tree, change_rate)
    return cog_data


def generate_one_meaning_outcome(tree, change_rate):
    edges = tree[1]
    root = tree[2]
    parents = tree[5]
    children = tree[6]

    one_meaning_data = dict()
    one_meaning_data[root] = 0
    max_cog_set = 0

    if root in children:
        current_level = children[root]
    else:
        current_level = set()

    while current_level:
        for node in current_level:
            parent = parents[node]

            branch_length = edges[(parent, node)]
            change_prob = 1 - math.exp(-change_rate * branch_length)
            parent_class = one_meaning_data[parent]
            r = rd.uniform(0, 1)
            if r < change_prob:
                one_meaning_data[node] = max_cog_set + 1
                max_cog_set += 1
            else:
                one_meaning_data[node] = parent_class
        current_level = tree_tools.get_node_set_children(tree, current_level)
    return one_meaning_data


# generates all possible (up to equivalence) outcomes of the multistate process on a given tree
def generate_all_outcomes(tree):
    nodes = tree[0]
    root = tree[2]
    parents = tree[5]
    children = tree[6]

    # calculate number of possible combinations of innovations and retentions that must be tested. For every non-root
    # node v, there is either a change or a retention at v which gives rise to 2**(num_nodes - 1) many combinations.
    num_nodes = len(nodes)
    num_combinations = 2 ** (num_nodes - 1)
    # find the digits of num_combinations in binary representation. A combination can be represented as a binary list of
    # length digits_num_comb
    digits_num_comb = num_nodes - 1

    combinations = [None] * num_combinations
    for i in range(num_combinations):
        # convert i to binary string
        binary_i = "{0:b}".format(i)
        # convert that to binary list
        combination = list(map(int, binary_i))
        # attach missing zeros at head
        combination = [0] * (digits_num_comb - len(combination)) + combination
        combinations[i] = combination

    # get non-root nodes and sort them. Given a certain combination, every non-root node will be assigned either an
    # innovation or a retention
    non_roots = list({node for node in nodes if node != root})
    non_roots.sort()

    combination_data_list = []

    for combination in combinations:
        # translate combination into a dictionary
        combination_dict = {non_roots[i]: combination[i] for i in range(len(non_roots))}

        # go through tree topology and generate cognates for given combination
        combination_data = dict()
        combination_data[root] = 0
        max_int_used = 0

        if root in children:
            current_level = children[root]
        else:
            current_level = set()

        while current_level:
            for node in current_level:
                parent = parents[node]
                if combination_dict[node] == 1:
                    combination_data[node] = max_int_used + 1
                    max_int_used += 1
                else:
                    combination_data[node] = combination_data[parent]
            current_level = tree_tools.get_node_set_children(tree, current_level)

        combination_data_list.append(combination_data)
    return combination_data_list



