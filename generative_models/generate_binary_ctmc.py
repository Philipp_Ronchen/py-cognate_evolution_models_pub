from lh_calculation import lh_binary_ctmc as lhb
import itertools
from small_tools import dict_tools
import math

# produces a list of all possible data sets (outcomes) of the binary CTMC model for a given set of leafs with exactly n
# many parallel values
def get_n_trait_data_sets(leaves, n):
    possible_data_lists = itertools.product([0, 1], repeat=n)
    possible_data_lists = list(map(list, possible_data_lists))
    leaf_possibilities = {leaf:possible_data_lists for leaf in leaves}
    print(leaf_possibilities)
    data_sets = dict_tools.dict_product(leaf_possibilities)
    return data_sets


# produces the sum of the likelihoods of the tree given all possible data sets with exactly n many parallel values.
# This should be 1. Used for test purposes
def calculate_possible_data_lh_sum(tree,b,d,root_freqs, n):
    leaves = tree[3]
    n_trait_data_sets = list(get_n_trait_data_sets(leaves, n))
    print(n_trait_data_sets)
    total_lh = 0
    for data_set in n_trait_data_sets:
        log_lh = lhb.calculate_parallel_data_log_lh(tree,b,d,root_freqs,data_set)
        lh = math.exp(log_lh)
        print(lh)
        total_lh += lh
    return total_lh