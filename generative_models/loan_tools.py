import copy
import random
from trees import tree_tools
from trees import tree_points as tpo
from generative_models import simulate_poisson_process as spp


def generate_loans(tree, loan_intensity):
    edges = tree[1]
    edge_points = dict()
    for edge in edges:
        edge_points[edge] = spp.generate_points_line(loan_intensity, edges[edge])
    to_points = {(edge, position) for edge in edges for position in edge_points[edge]}
    parallel_points = dict()
    for point in to_points:
        current_parallel_points = tpo.get_parallel_points(tree, point)
        # remove point itself
        current_edge = point[0]
        current_parallel_points = current_parallel_points.\
            difference(tpo.filter_points_by_edge(current_edge, current_parallel_points))
        if current_parallel_points:
            parallel_points[point] = current_parallel_points
        else:
            to_points.remove(point)
    from_point_dict = dict()
    for point in to_points:
        from_point_dict[point] = random.sample(list(parallel_points[point]), 1)[0]
    loan_list = [(to_point, from_point_dict[to_point]) for to_point in to_points]
    return loan_list




def change_topology_by_loan_list(tree, loan_list):
    new_tree = copy.deepcopy(tree)
    other_loans = copy.deepcopy(loan_list)
    while other_loans:
        loan = other_loans[0]
        other_loans = other_loans[1:]
        [new_tree, other_loans] = change_topology_by_loan(new_tree, loan, other_loans)
    return new_tree


# given a loan, changes the topology of the tree such that the resulting topology without a loan is equivalent to the
# old topology with the loan in terms of how cognates are substituted according to most cognate evolution models
# a loan is a tuple of the form (from_point, to_point), where from_point and to_point are given by their edge positions
# an edge_points_arg is a dictionary that assigns to all edges a set of points (as positions on the edge), edge_points are
# assigned reasonably in the adjusted topology
def change_topology_by_loan(tree, loan, other_loans):
    edges = tree[1]
    (from_p, to_p) = loan
    (from_edge, from_p_dist_from_parent) = from_p
    (to_edge, to_p_dist_from_parent) = to_p
    from_p_parent = from_edge[0]
    from_p_child = from_edge[1]
    to_p_parent = to_edge[0]
    to_p_child = to_edge[1]
    from_p_dist_from_child = edges[from_edge] - from_p_dist_from_parent
    to_p_dist_from_child = edges[to_edge] - to_p_dist_from_parent

    new_tree_edges = copy.deepcopy(edges)
    new_node_0 = "(" + to_p_parent + "-" + to_p_child + "_-)"
    new_node_1 = "(" + to_p_parent + "-" + to_p_child + "_+)"
    new_edge_0 = (to_p_parent, new_node_0)
    new_edge_1 = (from_p_parent, new_node_1)
    new_edge_2 = (new_node_1, from_p_child)
    new_edge_3 = (new_node_1, to_p_child)
    del new_tree_edges[from_edge]
    del new_tree_edges[to_edge]
    new_tree_edges[new_edge_0] = to_p_dist_from_parent
    new_tree_edges[new_edge_1] = from_p_dist_from_parent
    new_tree_edges[new_edge_2] = from_p_dist_from_child
    new_tree_edges[new_edge_3] = to_p_dist_from_child

    adjusted_other_loans = list()
    for (o_from_p, o_to_p) in other_loans:
        (o_from_edge, o_from_p_dist_from_parent) = o_from_p
        (o_to_edge, o_to_p_dist_from_parent) = o_to_p
        (new_o_from_edge, new_o_from_p_dist_from_parent) = (o_from_edge, o_from_p_dist_from_parent)
        (new_o_to_edge, new_o_to_p_dist_from_parent) = (o_to_edge, o_to_p_dist_from_parent)
        if o_from_edge == to_edge:
            if o_from_p_dist_from_parent < to_p_dist_from_parent:
                new_o_from_edge = new_edge_0
            else:
                new_o_from_edge = new_edge_3
                new_o_from_p_dist_from_parent = o_from_p_dist_from_parent - to_p_dist_from_parent
        if o_from_edge == from_edge:
            if o_from_p_dist_from_parent < from_p_dist_from_parent:
                new_o_from_edge = new_edge_1
            else:
                new_o_from_edge = new_edge_2
                new_o_from_p_dist_from_parent = o_from_p_dist_from_parent - from_p_dist_from_parent
        new_o_from_p = (new_o_from_edge, new_o_from_p_dist_from_parent)
        if o_to_edge == to_edge:
            if o_to_p_dist_from_parent < to_p_dist_from_parent:
                new_o_to_edge = new_edge_0
            else:
                new_o_to_edge = new_edge_3
                new_o_to_p_dist_from_parent = o_to_p_dist_from_parent - to_p_dist_from_parent
        if o_to_edge == from_edge:
            if o_to_p_dist_from_parent < from_p_dist_from_parent:
                new_o_to_edge = new_edge_1
            else:
                new_o_to_edge = new_edge_2
                new_o_to_p_dist_from_parent = o_to_p_dist_from_parent - from_p_dist_from_parent
        new_o_to_p = (new_o_to_edge, new_o_to_p_dist_from_parent)
        adjusted_other_loans.append((new_o_from_p, new_o_to_p))
        # new_edge_points = copy.deepcopy(edge_points)
        # new_edge_points[new_edge_0] = {p for p in edge_points[to_edge] if p < to_p_dist_from_parent}
        # new_edge_points[new_edge_1] = {p for p in edge_points[from_edge] if p < from_p_dist_from_parent}
        # new_edge_points[new_edge_2] = {p - from_p_dist_from_parent for p in edge_points[from_edge] if p >= from_p_dist_from_parent}
        # new_edge_points[new_edge_3] = {p - to_p_dist_from_parent for p in edge_points[to_edge] if p >= to_p_dist_from_parent}
        # del new_edge_points[from_edge]
        # del new_edge_points[to_edge]
        # new_edge_points_output.append(new_edge_points)

    new_tree = tree_tools.get_tree_from_edges(new_tree_edges)
    return [new_tree, adjusted_other_loans]
