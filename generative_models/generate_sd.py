import itertools
import math
import random as rd
from generative_models import simulate_poisson_process as spp
from scipy.stats import poisson
from more_itertools import powerset
from lh_calculation import lh_stochastic_dollo as lhsd
from trees import tree_tools as tt


# generates an outcome of the Stochastic Dollo model in the traits data format (assigns to every node, including
# internals, a set of integers representing traits
def generate_outcome(tree, b, d):
    edges = tree[1]
    root = tree[2]
    parents = tree[5]
    node_traits = dict()
    num_root_traits = poisson.rvs(b / d, 1)
    node_traits[root] = set(range(num_root_traits))
    max_trait_num = num_root_traits - 1

    levels = tt.get_downward_levels(tree)
    for level in levels[1:]:
        for node in level:
            parent = parents[node]
            edge_length = edges[(parent, node)]
            surviving_parent_traits = simulate_along_edge_survivals(edge_length, node_traits[parent], d)

            # simulate trait births along edge and their survival
            birth_positions = spp.generate_points_line(b, edge_length)
            num_surviving_births = simulate_edge_birth_survivals(edge_length, birth_positions, d)
            surviving_births = set(range(max_trait_num, max_trait_num + num_surviving_births))
            max_trait_num += num_surviving_births

            node_traits[node] = surviving_parent_traits.union(surviving_births)
    return node_traits


# given the length of an edge, and a set of traits present at the parent node, simulates the set of traits still present
# at the child node
def simulate_along_edge_survivals(edge_length, trait_set, d):
    thinned_trait_set = set()
    survival_prob = math.exp(- d * edge_length)
    for trait in trait_set:
        r = rd.uniform(0, 1)
        if r < survival_prob:
            thinned_trait_set.add(trait)
    return thinned_trait_set


# given the length of an edge, and a number of trait births given by their positions on the edge, simulates the number
# of the newborn traits that survive until the child node. Returns an integer
def simulate_edge_birth_survivals(edge_length, birth_point_positions, d):
    num_survivals = 0
    for position in birth_point_positions:
        dist_to_child = edge_length - position
        survival_prob = math.exp(- d * dist_to_child)
        r = rd.uniform(0, 1)
        if r < survival_prob:
            num_survivals += 1
    return num_survivals


# produces a list of all possible data sets (outcomes) of the Stochastic Dollo model on tree with exactly n many
# registered traits
def get_n_trait_data_sets(tree, n):
    leaves = tree[3]
    all_patterns = list(map(frozenset, list(powerset(leaves))))
    possible_patterns = all_patterns[1:]
    possible_patterns_repeated = [possible_patterns] * n
    product = itertools.product(*possible_patterns_repeated)
    converted_product = list(map(list, list(product)))
    data_sets = converted_product
    return data_sets


# produces a list of all possible data sets on tree with up to n many registered traits
def get_possible_data_sets(tree, n):
    data_sets_by_length_list = [0]*(n+1)
    for i in range(n+1):
        data_sets_by_length_list[i] = get_n_trait_data_sets(tree, i)
    data_sets = list(itertools.chain(*data_sets_by_length_list))
    print(data_sets)
    return data_sets


# produces the sum of the likelihoods of the tree given all possible data sets with up to n many registered traits.
# This should approach 1 when the number of traits grows
def calculate_possible_data_lh_sum(tree,b,d,n):
    data_sets = get_possible_data_sets(tree,n)
    lh_sum = 0
    for patterns in data_sets:
        lh = lhsd.compute_lh(tree,b,d,patterns, False)
        lh_sum += lh
    return lh_sum

