from trees import tree_points as tpo
import random

def initialise_point_path(from_root_edge):
    first_point = (from_root_edge, 0)
    first_value = 0
    ordered_points = [first_point]
    from_root_distances = {first_point:0}
    point_values = {first_point:first_value}
    value_weights = dict()
    return [ordered_points, from_root_distances, point_values, value_weights]


# adds a given point and a value to a point path. If the point type is "node" or "change", the value is determined,
# if it is reawake it is sampled with respect the value_weights.
# caution: function modifies input arguments
def add_point_and_value_to_point_path(tree, new_point, point_type, ordered_points, from_root_distances, point_values,
                                      value_weights, max_assigned_value):
    previous_point = ordered_points[-1]
    add_to_point_path(tree, new_point, ordered_points, from_root_distances, point_values, value_weights)
    new_max_assigned_value = add_sample_point_value_to_point_path(new_point, previous_point, point_type, point_values,
                                                                  value_weights, max_assigned_value)
    return new_max_assigned_value


# adds a new point (wich is further from the root than all points in ordered points) to the list ordered_points and
# to the dictionary from_root_distances. It does not assign a value to the new point, however it updates the value_weight
# of the value of the  previous point (length of segment of the tree that is associated to the value).
# caution: function modifies input arguments
def add_to_point_path(tree, new_point, ordered_points, from_root_distances, point_values, value_weights):
    previous_point = ordered_points[-1]
    new_point_from_root_distance = tpo.get_root_distance_from_edge_position(tree, new_point)
    ordered_points.append(new_point)
    from_root_distances[new_point] = new_point_from_root_distance

    # update the value weights for the value of the previous point
    previous_point_from_root_distance = from_root_distances[previous_point]
    previous_point_value = point_values[previous_point]
    new_segment_length = new_point_from_root_distance - previous_point_from_root_distance
    if previous_point_value in value_weights:
        value_weights[previous_point_value] += new_segment_length
    else:
        value_weights[previous_point_value] = new_segment_length

    return None


# adds the value of a new point to point_values
# caution: function modifies input argument point_values
def add_sample_point_value_to_point_path(new_point, previous_point, point_type, point_values, value_weights,
                                  max_assigned_value):
    if point_type == "change":
        new_value = max_assigned_value + 1
        new_max_assigned_value = new_value
    elif point_type == "reawake":
        print("sample value here for reawake")
        print("value weights:")
        print(value_weights)
        new_value = sample_reawake_point_value(value_weights)
        print("sampled value")
        print(new_value)
        new_max_assigned_value = max_assigned_value
    elif point_type == "node":
        new_value = point_values[previous_point]
        new_max_assigned_value = max_assigned_value
    else:
        raise ValueError("point_type has to be 'change', 'reawake' or 'node'")
    point_values[new_point] = new_value
    return new_max_assigned_value


def sample_reawake_point_value(value_weights):
    value_list = list(value_weights.keys())
    value_weights = [value_weights[value] for value in value_list]
    sample_value = random.choices(value_list, value_weights, k=1)[0]
    return sample_value

# def add_change_point_to_point_path(tree, new_point, ordered_points, from_root_distances, point_values,
#                                    value_weights,  max_assigned_value):
#     new_point_from_root_distance = tpo.get_root_distance_from_edge_position(tree, new_point)
#     previous_point = ordered_points[-1]
#     previous_point_from_root_distance = from_root_distances[previous_point]
#     previous_point_value = point_values[previous_point]
#     new_value = max_assigned_value + 1
#
#     new_ordered_points = ordered_points
#     new_ordered_points.append(new_point)
#     new_from_root_distances = from_root_distances
#     new_from_root_distances[new_point] = new_point_from_root_distance
#     new_point_values = point_values
#     new_point_values[new_point] = new_value
#     new_value_weights = value_weights
#     new_value_weights[previous_point_value] = new_point_from_root_distance - previous_point_from_root_distance
#     new_max_assigned_value = new_value
#     return [new_ordered_points, new_from_root_distances, new_point_values, new_value_weights, new_max_assigned_value]
#
#
# def add_reawake_point_to_point_path(tree, new_point, ordered_points, from_root_distances, point_values,
#                                    value_weights)
