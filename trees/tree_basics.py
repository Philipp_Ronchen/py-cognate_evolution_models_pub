""""
Description of data format: A tree is a list of the form [nodes,edges,root,leaves,internals,parents,children],  where: 
 - node is a set (of strings for example).
 - edges is a dictionary indexed by tuples (a,b) where a and b are in nodes. a is the parent and a is the child node.
   The value of (a,b) is its edge weight, if edge weights are unspecified, it is -1.
 - root is an element of nodes and specifies the tree root.
 - leaves is the subset of nodes that are leaves in the tree. It can be computed by get_leaves.
 - internals is the subset of nodes that are not leaves in the tree. It can be computed by get_internals.
 - parents is a dictionary, associating to every non-root node its parent. It can be computed by get_parents.
 - children is a dictionary, associating to every non-leaf node a set of its children. It can be computed by 
   get_children.
We have: tree[0] = nodes,
         tree[1] = edges
         tree[2] = root
         tree[3] = leaves
         tree[4] = internals
         tree[5] = parents
         tree[6] = children
nodes and edges together always contain the complete information about the tree. If the tree is not a single, edges 
contains the complete information about the tree. edges is the only data variable that stores information about branch 
lengths and not only tree topology.
"""


# returns a set of all leaf nodes
def get_leaves(nodes, edges):
    parent_nodes = {edge[0] for edge in edges}
    leaves = {node for node in nodes if node not in parent_nodes}
    return leaves


# returns a set of all internal nodes
def get_internals(edges):
    internals = set([edge[0] for edge in edges])
    return internals


# returns a dictionary, linking to every non-root its parent
def get_parents(edges):
    parents = {edge[1]:edge[0] for edge in edges}
    return parents


# returns a dictionary, linking to every non-leaf node a set of its children
def get_children(edges):
    parent_nodes = set([edge[0] for edge in edges])
    children = {node:set() for node in parent_nodes}
    for edge in edges:
        parent = edge[0]
        child = edge[1]
        children[parent].add(child)
    return children


# returns the set of nodes of the tree, from its edges
def get_nodes(edges):
    nodes = set([edge[0] for edge in edges]).union(set([edge[1] for edge in edges]))
    return nodes


# returns the root of the tree
def get_root(nodes,edges):
    parents = get_parents(edges)
    non_parents = {node for node in nodes if node not in parents}
    root = list(non_parents)[0]
    return root




