from trees import tree_tools


# gives the set of nodes on the path from node to the root
def get_root_path_nodes(topology, node):
    parents = topology[5]
    current_node = node
    path_nodes = {current_node}
    while current_node in parents:
        current_node = parents[current_node]
        path_nodes = path_nodes.union({current_node})
    return path_nodes


# returns the set of nodes on the path from node_1 to node_2
def get_path_nodes(topology,node_1,node_2):
    # get the most recent common ancestor of the nodes
    mrca = tree_tools.get_mrca(topology, node_1, node_2)
    mrca_root_path = get_root_path_nodes(topology, mrca)
    node_1_root_path = get_root_path_nodes(topology, node_1)
    node_2_root_path = get_root_path_nodes(topology, node_2)
    path = node_1_root_path.union(node_2_root_path).difference(mrca_root_path).union({mrca})
    return path


# gives a path from node to the root. A path is a dictionary of edges
def get_node_to_root_path(tree, node):
    edges = tree[1]
    parents = tree[5]
    path = dict()
    current_node = node
    while current_node in parents:
        parent = parents[current_node]
        path[(parent,current_node)] = edges[(parent,current_node)]
        current_node = parent
    return path


# gives a dictionary of paths with one entry for every leaf. A path is a dictionary of edges from leaf to root
def get_leaf_root_paths(tree):
    edges = tree[1]
    leaves = tree[3]
    parents = tree[5]
    paths = {leaf:{} for leaf in leaves}
    for leaf in leaves:
        node = leaf
        while node in parents:
            parent = parents[node]
            paths[leaf][(parent,node)] = edges[(parent,node)]
            node = parent
    return paths


# gives the path from a given node to the root as a list of edges (without specifying the edge lengths)
def get_ordered_node_to_root_path(topology, node):
    root = topology[2]
    parents = topology[5]
    path = list()
    current_node = node
    while current_node in parents:
        parent = parents[current_node]
        path.append((parent,current_node))
        current_node = parent
    return path


# gives the path from a given node to the root as a list of edges (without specifying the edge lengths)
def get_ordered_root_to_node_path(topology, node):
    node_to_root_path = get_ordered_node_to_root_path(topology, node)
    root_to_node_path = list(reversed(node_to_root_path))
    return root_to_node_path
