from trees import tree_basics as tb
from trees import tree_tools as tt
import itertools
import copy


# gives a list of all possible trees with n labelled leaves (keeping track of which leaf has been appended to
# which branch)
def get_binary_n_leaf_trees(n):
    leaves = {"l"+str(i) for i in range(n)}
    sorted_leaves = sorted(list(leaves))
    internals = {"i"+str(j) for j in range(n-1)}
    sorted_internals = sorted(list(internals))
    nodes = leaves.union(internals)
    ways_to_add = list()
    for k in range(2,n+1):
        ways_to_add.append(range(2*k-3))
    print("ways to add")
    print(ways_to_add)
    combs = list(itertools.product(*ways_to_add))
    print("combs")
    print(combs)
    trees = list()
    for comb in list(combs):
        print("comb")
        print(comb)
        sorted_edges = list()
        current_root = sorted_leaves[0]
        for k in range(n-1):
            print("k")
            print(k)
            print("sorted edges")
            print(sorted_edges)
            place_to_add = comb[k]
            print("place_to_add")
            print(place_to_add)
            next_leaf = sorted_leaves[k+1]
            next_internal = sorted_internals[k]
            print("next leaf")
            print(next_leaf)
            print("next internal")
            print(next_internal)
            if place_to_add == ways_to_add[k][-1]:
                print("adding root")
                new_edge_1 = (next_internal,current_root)
                new_edge_2 = (next_internal,next_leaf)
                sorted_edges += [new_edge_1, new_edge_2]
                sorted_edges.sort()
                current_root = next_internal
            else:
                edge_to_split = sorted_edges[place_to_add]
                print("edge to split")
                print(edge_to_split)
                old_parent = edge_to_split[0]
                old_child = edge_to_split[1]
                new_edge_1 = (next_internal,old_child)
                new_edge_2 = (old_parent, next_internal)
                new_edge_3 = (next_internal,next_leaf)
                sorted_edges.remove(edge_to_split)
                sorted_edges += [new_edge_1, new_edge_2, new_edge_3]
                sorted_edges.sort()
        print(sorted_edges)
        new_nodes = copy.copy(nodes)
        new_edges = {edge:-1 for edge in sorted_edges}
        new_root = current_root
        new_leaves = copy.copy(leaves)
        new_internals = copy.copy(internals)
        new_parents = tb.get_parents(new_edges)
        new_children = tb.get_children(new_edges)
        new_tree = [new_nodes, new_edges, new_root, new_leaves, new_internals, new_parents, new_children]
        new_tree = rename_nodes(new_tree, new_leaves)
        trees.append(new_tree)
    return trees




# renames the nodes of the tree such that they are enumerated level-wise as "n0", "n1" (with the root having the
# highest number), keeps the node names in node_names_to_keep
def rename_nodes(tree, node_names_to_keep):
    old_nodes = tree[0]
    old_edges = tree[1]
    old_root = tree[2]
    old_leaves = tree[3]
    old_internals = tree[4]
    old_parents = tree[5]
    old_children = tree[6]
    levels = tt.get_levels(tree)
    names = dict()
    i = 0
    for level in levels:
        for node in level:
            if node in node_names_to_keep:
                names[node] = node
            else:
                names[node] = "n"+str(i)
                i += 1
    new_nodes = {names[node] for node in old_nodes}
    new_edges = {(names[edge[0]],names[edge[1]]):old_edges[edge] for edge in old_edges}
    new_root = names[old_root]
    new_leaves = {names[node] for node in old_leaves}
    new_internals = {names[node] for node in old_internals}
    new_parents = {names[node]:names[old_parents[node]] for node in old_parents}
    new_children = {names[node]:{names[child] for child in old_children[node]} for node in old_children}
    new_tree = [new_nodes, new_edges, new_root, new_leaves, new_internals, new_parents, new_children]
    return new_tree


# renames some of the tree nodes as specified in the dict new_names
def rename_nodes_to(tree, new_names):
    old_nodes = tree[0]
    old_edges = tree[1]
    old_root = tree[2]
    old_leaves = tree[3]
    old_internals = tree[4]
    old_parents = tree[5]
    old_children = tree[6]

    names = new_names
    for node in old_nodes:
        if node not in names:
            names[node] = node
    new_nodes = {names[node] for node in old_nodes}
    new_edges = {(names[edge[0]],names[edge[1]]):old_edges[edge] for edge in old_edges}
    new_root = names[old_root]
    new_leaves = {names[node] for node in old_leaves}
    new_internals = {names[node] for node in old_internals}
    new_parents = {names[node]:names[old_parents[node]] for node in old_parents}
    new_children = {names[node]:{names[child] for child in old_children[node]} for node in old_children}
    new_tree = [new_nodes, new_edges, new_root, new_leaves, new_internals, new_parents, new_children]
    return new_tree

