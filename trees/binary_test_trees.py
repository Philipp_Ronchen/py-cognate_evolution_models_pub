from trees import tree_basics

# 2 leaf tree (cherry tree)
nodes = {"a","b","c"}
edges = {("c","a"):1,("c","b"):1}
root = "c"
leaves = tree_basics.get_leaves(nodes, edges)
internals = tree_basics.get_internals(edges)
parents = tree_basics.get_parents(edges)
children = tree_basics.get_children(edges)

b2l_tree = [nodes, edges, root, leaves, internals, parents, children]


# 3 leaf tree
nodes = {"a","b","c","d","e"}
edges = {("d","a"):1,("d","b"):1,("e","d"):1,("e","c"):2}
root = "e"
leaves = tree_basics.get_leaves(nodes, edges)
internals = tree_basics.get_internals(edges)
parents = tree_basics.get_parents(edges)
children = tree_basics.get_children(edges)

b3l_tree = [nodes, edges, root, leaves, internals, parents, children]


# 4 leaf tree 1
nodes = {"a","b","c","d","e", "f", "g"}
edges = {("e","a"):1,("e","b"):1,("f","e"):1,("f","c"):2, ("g","d"):3, ("g","f"):1}
root = "g"
leaves = tree_basics.get_leaves(nodes, edges)
internals = tree_basics.get_internals(edges)
parents = tree_basics.get_parents(edges)
children = tree_basics.get_children(edges)

b4l_tree_1 = [nodes, edges, root, leaves, internals, parents, children]


# 4 leaf tree 2
nodes = {"a","b","c","d","e", "f", "g"}
edges = {("e","a"):1,("e","b"):1,("f","c"):1,("f","d"):1, ("g","e"):1, ("g","f"):1}
root = "g"
leaves = tree_basics.get_leaves(nodes, edges)
internals = tree_basics.get_internals(edges)
parents = tree_basics.get_parents(edges)
children = tree_basics.get_children(edges)

b4l_tree_2 = [nodes, edges, root, leaves, internals, parents, children]


# 5 leaf tree 1
nodes = {"a","b","c","d","e", "f", "g", "h", "i"}
edges = {("f","a"):1,("f","b"):1,("g","c"):2,("g","f"):1, ("h","d"):3, ("h","g"):1, ("i","e"):4, ("i","h"):1}
root = "i"
leaves = tree_basics.get_leaves(nodes, edges)
internals = tree_basics.get_internals(edges)
parents = tree_basics.get_parents(edges)
children = tree_basics.get_children(edges)

b5l_tree_1 = [nodes, edges, root, leaves, internals, parents, children]


# 5 leaf tree 2
nodes = {"a","b","c","d","e", "f", "g", "h", "i"}
edges = {("f","a"):1,("f","b"):1,("g","c"):2,("g","f"):1, ("h","d"):1, ("h","e"):1, ("i","h"):2, ("i","g"):1}
root = "i"
leaves = tree_basics.get_leaves(nodes, edges)
internals = tree_basics.get_internals(edges)
parents = tree_basics.get_parents(edges)
children = tree_basics.get_children(edges)

b5l_tree_2 = [nodes, edges, root, leaves, internals, parents, children]


# 5 leaf tree 3
nodes = {"a","b","c","d","e", "f", "g", "h", "i"}
edges = {("f","a"):1,("f","b"):1,("g","c"):1,("g","d"):1, ("h","f"):1, ("h","g"):1, ("i","e"):3, ("i","h"):1}
root = "i"
leaves = tree_basics.get_leaves(nodes, edges)
internals = tree_basics.get_internals(edges)
parents = tree_basics.get_parents(edges)
children = tree_basics.get_children(edges)

b5l_tree_3 = [nodes, edges, root, leaves, internals, parents, children]
