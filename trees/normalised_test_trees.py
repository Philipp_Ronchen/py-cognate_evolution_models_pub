from trees import tree_basics
import math


# normalised tree. For a death/change rate of 1 the survival rate along a branch of length t is 0.5 and the survival rate
# along a branch of length 2t is 0.25
t = -math.log(0.5)
nodes = {"a","b","c","d","e"}
edges = {("d","a"):t,("d","b"):t,("e","c"):2*t,("e","d"):t}

root = "e"
leaves = tree_basics.get_leaves(nodes, edges)
internals = tree_basics.get_internals(edges)
parents = tree_basics.get_parents(edges)
children = tree_basics.get_children(edges)

normalised_tree_1 = [nodes, edges, root, leaves, internals, parents, children]

# infinite edge tree. A tree consisting of a single edge of infinite length
t = math.inf
nodes = {"a","b"}
edges = {("b","a"):t}

root = "b"
leaves = tree_basics.get_leaves(nodes, edges)
internals = tree_basics.get_internals(edges)
parents = tree_basics.get_parents(edges)
children = tree_basics.get_children(edges)

infinite_edge_tree = [nodes, edges, root, leaves, internals, parents, children]
