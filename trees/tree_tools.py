import copy
import math
from trees import tree_basics, tree_paths


# gives the most recent common ancestor of two nodes in the tree
def get_mrca(tree,node_1,node_2):
    parents = tree[5]
    extension_1 = {node_1}
    extension_2 = {node_2}
    current_node_a = node_1
    current_node_b = node_2
    while not extension_1.intersection(extension_2):
        if current_node_a in parents:
            new_node_a = parents[current_node_a]
            extension_1 = extension_1.union({new_node_a})
            current_node_a = new_node_a
        else:
            pass
        if current_node_b in parents:
            new_node_b = parents[current_node_b]
            extension_2 = extension_2.union({new_node_b})
            current_node_b = new_node_b
        else:
            pass
    mrca = list(extension_1.intersection(extension_2))[0]
    return mrca


# gives the most recent common ancestor of a non-empty set of nodes in the tree
def get_set_mrca(tree,node_set):
    node_list = list(node_set)
    if len(node_list) == 0:
        raise ValueError("node set needs to be non-empty")
    elif len(node_list) == 1:
        return node_list[0]
    else:
        node_1 = node_list[0]
        node_2 = node_list[1]
        two_node_mrca = get_mrca(tree,node_1,node_2)
        reduced_node_set = node_set.difference({node_1, node_2}).union({two_node_mrca})
        return get_set_mrca(tree,reduced_node_set)


# gives a set of all the children of nodes in node_set
def get_node_set_children(tree, node_set):
    children = tree[6]
    node_set_children = set()
    for node in node_set:
        if node in children:
            node_set_children = node_set_children.union(children[node])
    return node_set_children


# gives a set of all the children of nodes in node_set which are not already in node_set
def get_new_node_set_children(tree, node_set):
    children = tree[6]
    node_set_children = get_node_set_children(tree, node_set)
    new_node_set_children = node_set_children.difference(node_set)
    return new_node_set_children


# gives the set of nodes of the subtree with "node" as root
def get_subtree_nodes(tree, node):
    subtree_nodes = set()
    current_level = {node}
    while current_level:
        subtree_nodes = subtree_nodes.union(current_level)
        current_level = get_node_set_children(tree,current_level)
    return subtree_nodes


# gives the set of parents of the nodes in node_set which have all there children in node_set
def get_node_set_parents(tree, node_set):
    parents = tree[5]
    children = tree[6]
    node_set_parents = set()
    for node in node_set:
        if node in parents:
            if children[parents[node]].issubset(node_set):
                node_set_parents.add(parents[node])
    return node_set_parents


# gives the set of parents of the nodes in node_set, which have all there children in node_set and which are not
# already in node_set
def get_new_node_set_parents(tree, node_set):
    node_set_parents = get_node_set_parents(tree, node_set)
    new_node_set_parents = node_set_parents.difference(node_set)
    return new_node_set_parents


# gives all the nodes contained in the edges in edge_set
def get_nodes_from_edge_set(edge_set):
    node_set = {node for edge in edge_set for node in edge}
    return node_set


# gives a list of the node levels of the tree, starting at the leaves. For level n, the longest path from some leaf to a
# node in level n has length n
def get_levels(tree):
    nodes = tree[0]
    leaves = tree[3]
    levels = list()
    current_level = set()
    if nodes:
        current_level = leaves
        used_nodes = leaves
        levels.append(current_level)
    while current_level:
        current_level = get_new_node_set_parents(tree,used_nodes)
        if current_level:
            levels.append(current_level)
            used_nodes = used_nodes.union(current_level)
    return levels


# gives a list of the downward node levels of the tree, starting at the root. For level n, the unique path from the
# root to a node in level n has length n
def get_downward_levels(tree):
    root = tree[2]
    levels = list()
    current_level = {root}
    used_nodes = {root}
    levels.append(current_level)
    while current_level:
        current_level = get_new_node_set_children(tree, used_nodes)
        if current_level:
            levels.append(current_level)
            used_nodes = used_nodes.union(current_level)
    return levels


# adds an edge to the root of the tree, having an "adam node" as its parents. The length if the edge is infinity
def add_adam_node(tree):
    nodes = tree[0]
    edges = tree[1]
    old_root = tree[2]
    adam_node = "adam"
    new_nodes = copy.copy(nodes)
    new_nodes.add(adam_node)
    new_edges = copy.copy(edges)
    new_edges[(adam_node, old_root)] = math.inf
    new_root = adam_node
    leaves = tree_basics.get_leaves(new_nodes, new_edges)
    internals = tree_basics.get_internals(new_edges)
    parents = tree_basics.get_parents(new_edges)
    children = tree_basics.get_children(new_edges)
    adam_tree = [new_nodes, new_edges, new_root, leaves, internals, parents, children]
    return adam_tree


# reconstruct a tree from its edges
def get_tree_from_edges(edges):
    nodes = tree_basics.get_nodes(edges)
    root = tree_basics.get_root(nodes,edges)
    leaves = tree_basics.get_leaves(nodes,edges)
    internals = tree_basics.get_internals(edges)
    parents = tree_basics.get_parents(edges)
    children = tree_basics.get_children(edges)
    edges_copied = copy.copy(edges)
    tree = [nodes, edges_copied, root, leaves, internals, parents, children]
    return tree


# gives a dictionary assigning to every node in the tree its age. The leaf that is furthest away from the root is
# assigned age 0
def get_node_ages(tree):
    nodes = tree[0]
    root_paths = {node:tree_paths.get_node_to_root_path(tree, node) for node in nodes}
    path_lengths = {node:sum(root_paths[node].values()) for node in nodes}
    root_age = max(path_lengths.values())
    node_ages = {node:(root_age - path_lengths[node]) for node in nodes}
    return node_ages


# TO DO: IS THIS NEEDED?
# converts a tree using multiple precision branch lengths (mpfr format) to a tree having float branch lenghts
def get_float_tree(tree):
    new_tree = tree.copy()
    edges = copy.deepcopy(new_tree[1])
    for edge in edges:
        edges[edge] = float(edges[edge])
    new_tree[1] = edges
    return new_tree