# implements functions that store and transform points on the tree, that is positions that are not necessarily nodes
from trees import tree_paths as tp
import copy


# given a point on the tree indicates the edge on which the point falls, and where on the edge it falls.
# A point is indicated by some (not necessarily unique) leaf such that the point
# lies on the path from the root to the leaf, and by the distance of the point from the root.
# Returns a point as a tuple (edge, edge_postion)
# In case the point lies on the junction of two edges on the leaf path, the lower edge (closer to the leaf) is given
# The edge_position that is returned is the distance from the point to the parent node of the edge.
def get_edge_position_from_leaf_path_position(tree, leaf, distance_from_root):
    edges = tree[1]
    leaf_path = tp.get_ordered_root_to_node_path(tree, leaf)
    current_edge = leaf_path[0]
    i = 0
    current_dist = edges[current_edge]
    while current_dist <= distance_from_root:
        i += 1
        current_edge = leaf_path[i]
        current_dist += edges[current_edge]
    edge_position = distance_from_root + edges[current_edge] - current_dist
    return (current_edge, edge_position)


# gives the distance of a point from the root given its edge position
def get_root_distance_from_edge_position(tree, point):
    (edge, edge_position) = point
    edges = tree[1]
    ancestor_node = edge[0]
    root_to_ancestor_path = tp.get_ordered_root_to_node_path(tree, ancestor_node)
    path_edge_lengths = list(map(lambda x: edges[x], root_to_ancestor_path))
    root_distance = sum(path_edge_lengths) + edge_position
    return root_distance


# returns the sublist of points that lie on the given edge
def filter_points_by_edge(edge, point_list):
    filtered_points = list(filter(lambda x: x[0] == edge, point_list))
    return filtered_points


# gives a set of all the points that are parallel to the input point, that is have the same distance from the root.
# The input point is included.
def get_parallel_points(tree, point):
    leaves = copy.deepcopy(tree[3])
    distance_from_root = get_root_distance_from_edge_position(tree, point)
    parallel_points_with_reps = \
        list(map(lambda x: get_edge_position_from_leaf_path_position(tree, x, distance_from_root), leaves))
    print("parallel points with reps")
    print(list(parallel_points_with_reps))
    parallel_edges = set(map(lambda x: x[0], parallel_points_with_reps))
    print("parallel edges")
    print(parallel_edges)
    # assign to every parallel edge the list of points that fall on this edge (might be several that differ by some
    # epsilon because of floating point arithmetic problems)
    positions_dict = dict()
    for edge in parallel_edges:
        positions_dict[edge] = [point[1] for point in parallel_points_with_reps if point[0] == edge]
    print("positions dict")
    print(positions_dict)
    parallel_points = {(edge, positions_dict[edge][0]) for edge in parallel_edges}
    return parallel_points


def get_point_path_value_weights(ordered_points, distances_from_root, before_point_values):
    value_weights = dict()
    first_point = ordered_points[0]
    first_distance = distances_from_root[first_point]
    first_value = before_point_values[first_point]
    value_weights[first_value] = first_distance
    for i in range(1, len(ordered_points)):
        point = ordered_points[i]
        previous_point = ordered_points[i-1]
        distance = distances_from_root[point] - distances_from_root[previous_point]
        value = before_point_values[point]
        if value in value_weights:
            value_weights[value] += distance
        else:
            value_weights[value] = distance
    return value_weights

    return None

