from trees import tree_basics


# non-binary 4 leaf tree
nodes = {"a","b","b2","c","d","e"}
edges = {("d","a"):1,("d","b"):1,("d","b2"):1,("e","d"):1,("e","c"):2}
root = "e"
leaves = tree_basics.get_leaves(nodes, edges)
internals = tree_basics.get_internals(edges)
parents = tree_basics.get_parents(edges)
children = tree_basics.get_children(edges)

nb4l_tree = [nodes, edges, root, leaves, internals, parents, children]


# non-binary five leaf tree 1
nodes = {"a","b","c","d","e","f","g","h","i","j"}
edges = {("f","a"):1,("f","b"):1,("g","c"):1,("g","d"):1,("h","e"):2,("h","g"):1,("i","f"):3,("i","h"):2,("j","i"):2}
root = "j"
leaves = tree_basics.get_leaves(nodes, edges)
internals = tree_basics.get_internals(edges)
parents = tree_basics.get_parents(edges)
children = tree_basics.get_children(edges)

nb5l_tree_1 = [nodes, edges, root, leaves, internals, parents, children]


# non-binary 5 leaf tree 2
nodes = {"a","b","b2","c","d","e", "f", "g"}
edges = {("d","a"):1,("d","b"):1,("d","b2"):1,("e","d"):1,("e","c"):2, ("g","e"):1, ("g","f"):3}
root = "g"
leaves = tree_basics.get_leaves(nodes, edges)
internals = tree_basics.get_internals(edges)
parents = tree_basics.get_parents(edges)
children = tree_basics.get_children(edges)

nb5l_tree_2 = [nodes, edges, root, leaves, internals, parents, children]


# non-binary 5 leaf tree 3, polytomy with 5 leaves and one root
nodes = {"a","b","c","d","e","f"}
edges = {("f","a"):1,("f","b"):1,("f","c"):1,("f","d"):1,("f","e"):1}
root = "f"
leaves = tree_basics.get_leaves(nodes, edges)
internals = tree_basics.get_internals(edges)
parents = tree_basics.get_parents(edges)
children = tree_basics.get_children(edges)

nb5l_tree_3 = [nodes, edges, root, leaves, internals, parents, children]


# non-binary 7 leaf tree 1
nodes = {"a","b","c","d","e","f","g","h","i","j","k","l","m"}
edges = {("h","a"):1,("h","b"):1,("h","c"):1,("m","d"):5,("k","e"):3,("i","f"):2,("i","g"):2,("l","h"):3,("k","i"):1,
         ("l","j"):2,("m","k"):2,("m","l"):1}
root = "m"
leaves = tree_basics.get_leaves(nodes, edges)
internals = tree_basics.get_internals(edges)
parents = tree_basics.get_parents(edges)
children = tree_basics.get_children(edges)

nb7l_tree_1 = [nodes, edges, root, leaves, internals, parents, children]


# non-binary 7 leaf tree 2
nodes = {"a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q"}#,"r","s","t"}
edges = {("h","a"):1,("h","b"):1,("h","c"):1,("m","d"):5,("k","e"):3,("i","f"):2,("i","g"):2,("l","h"):3,("k","i"):1,
         ("l","j"):2,("m","k"):2,("m","l"):1,("n","m"):1,("o","n"):1,("p","o"):1,("q","p"):1}
            #,("r","q"):1,("s","r"):1, ("t","s"):1}
root = "q"
leaves = tree_basics.get_leaves(nodes, edges)
internals = tree_basics.get_internals(edges)
parents = tree_basics.get_parents(edges)
children = tree_basics.get_children(edges)

nb7l_tree_2 = [nodes, edges, root, leaves, internals, parents, children]


# non-binary 14 leaf tree
nodes = {"a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v"}
edges = {("q","a"):2,("q","b"):2,("q","c"):2,("q","d"):2,("o","e"):1,("o","f"):1,("p","g"):1,("p","h"):1,("r","i"):2,
         ("r","j"):2,("u","k"):3,("s","l"):2,("s","m"):2,("s","n"):2,("t","o"):2,("u","p"):2,("t","q"):1,("u","r"):1,
         ("v","s"):2,("v","t"):1,("v","u"):1}
root = "v"
leaves = tree_basics.get_leaves(nodes, edges)
internals = tree_basics.get_internals(edges)
parents = tree_basics.get_parents(edges)
children = tree_basics.get_children(edges)

nb14l_tree = [nodes, edges, root, leaves, internals, parents, children]


# non-binary 15 leaf tree
nodes = {"a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"}
edges = {("t","a"):2,("t","b"):2,("t","c"):2,("t","d"):2,("p","e"):1,("p","f"):1,("q","g"):1,("q","h"):1,("u","i"):2,
         ("u","j"):2,("r","k"):1,("r","l"):1,("s","m"):1,("s","n"):1,("v","o"):2,("w","p"):2,("x","q"):2,("v","r"):1,
         ("v","s"):1,("w","t"):1,("x","u"):1,("z","v"):3,("y","w"):1,("y","x"):1,("z","y"):1}
root = "z"
leaves = tree_basics.get_leaves(nodes, edges)
internals = tree_basics.get_internals(edges)
parents = tree_basics.get_parents(edges)
children = tree_basics.get_children(edges)

nb15l_tree = [nodes, edges, root, leaves, internals, parents, children]


# non-binary 75 leaf tree (contains repetitions of nb15l_tree as subtrees)
nodes = {"a0","b0","c0","d0","e0","f0","g0","h0","i0","j0","k0","l0","m0","n0","o0","p0","q0","r0","s0","t0","u0","v0",
         "w0","x0","y0","z0", "a1","b1","c1","d1","e1","f1","g1","h1","i1","j1","k1","l1","m1","n1","o1","p1","q1","r1",
         "s1","t1","u1","v1","w1","x1","y1","z1","a2","b2","c2","d2","e2","f2","g2","h2","i2","j2","k2","l2","m2","n2",
         "o2","p2","q2","r2","s2","t2","u2","v2","w2","x2","y2","z2","a3","b3","c3","d3","e3","f3","g3","h3","i3","j3",
         "k3","l3","m3","n3","o3","p3","q3","r3","s3","t3","u3","v3","w3","x3","y3","z3","a4","b4","c4","d4","e4","f4",
         "g4","h4","i4","j4","k4","l4","m4","n4","o4","p4","q4","r4","s4","t4","u4","v4","w4","x4","y4","z4","ro"}

edges = {("t0","a0"):2,("t0","b0"):2,("t0","c0"):2,("t0","d0"):2,("p0","e0"):1,("p0","f0"):1,("q0","g0"):1,
         ("q0","h0"):1,("u0","i0"):2,("u0","j0"):2,("r0","k0"):1,("r0","l0"):1,("s0","m0"):1,("s0","n0"):1,
         ("v0","o0"):2,("w0","p0"):2,("x0","q0"):2,("v0","r0"):1,("v0","s0"):1,("w0","t0"):1,("x0","u0"):1,
         ("z0","v0"):3,("y0","w0"):1,("y0","x0"):1,("z0","y0"):1, ("t1","a1"):2,("t1","b1"):2,("t1","c1"):2,
         ("t1","d1"):2,("p1","e1"):1,("p1","f1"):1,("q1","g1"):1,("q1","h1"):1,("u1","i1"):2,("u1","j1"):2,
         ("r1","k1"):1,("r1","l1"):1,("s1","m1"):1,("s1","n1"):1,("v1","o1"):2,("w1","p1"):2,("x1","q1"):2,
         ("v1","r1"):1,("v1","s1"):1,("w1","t1"):1,("x1","u1"):1,("z1","v1"):3,("y1","w1"):1,("y1","x1"):1,
         ("z1","y1"):1,("t2","a2"):2,("t2","b2"):2,("t2","c2"):2,("t2","d2"):2,("p2","e2"):1,("p2","f2"):1,
         ("q2","g2"):1,("q2","h2"):1,("u2","i2"):2,("u2","j2"):2,("r2","k2"):1,("r2","l2"):1,("s2","m2"):1,
         ("s2","n2"):1,("v2","o2"):2,("w2","p2"):2,("x2","q2"):2,("v2","r2"):1,("v2","s2"):1,("w2","t2"):1,
         ("x2","u2"):1,("z2","v2"):3,("y2","w2"):1,("y2","x2"):1,("z2","y2"):1,("t3","a3"):2,("t3","b3"):2,
         ("t3","c3"):2,("t3","d3"):2,("p3","e3"):1,("p3","f3"):1,("q3","g3"):1,("q3","h3"):1,("u3","i3"):2,
         ("u3","j3"):2,("r3","k3"):1,("r3","l3"):1,("s3","m3"):1,("s3","n3"):1,("v3","o3"):2,("w3","p3"):2,
         ("x3","q3"):2,("v3","r3"):1,("v3","s3"):1,("w3","t3"):1,("x3","u3"):1,("z3","v3"):3,("y3","w3"):1,
         ("y3","x3"):1,("z3","y3"):1,("t4","a4"):2,("t4","b4"):2,("t4","c4"):2,("t4","d4"):2,("p4","e4"):1,
         ("p4","f4"):1,("q4","g4"):1,("q4","h4"):1,("u4","i4"):2,("u4","j4"):2,("r4","k4"):1,("r4","l4"):1,
         ("s4","m4"):1,("s4","n4"):1,("v4","o4"):2,("w4","p4"):2,("x4","q4"):2,("v4","r4"):1,("v4","s4"):1,
         ("w4","t4"):1,("x4","u4"):1,("z4","v4"):3,("y4","w4"):1,("y4","x4"):1,("z4","y4"):1,
         ("ro","z0"):1,("ro","z1"):1,("ro","z2"):1,("ro","z3"):1,("ro","z4"):1}
root = "ro"
leaves = tree_basics.get_leaves(nodes, edges)
internals = tree_basics.get_internals(edges)
parents = tree_basics.get_parents(edges)
children = tree_basics.get_children(edges)

nb75l_tree = [nodes, edges, root, leaves, internals, parents, children]







