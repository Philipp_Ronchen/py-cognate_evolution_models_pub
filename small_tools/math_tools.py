import math


def inf_log10(x):
    if x > 0:
        return math.log10(x)
    elif x == 0:
        return -math.inf
    else:
        raise ValueError("math domain error")


def inf_log(x):
    if x > 0:
        return math.log(x)
    elif x == 0:
        return -math.inf
    else:
        raise ValueError("math domain error")
