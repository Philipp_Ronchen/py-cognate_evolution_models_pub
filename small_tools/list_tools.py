import math

# gives a list of all binary lists (lists of 0's and 1's of) a given length
def get_all_binary_lists(list_length):
    if list_length == 0:
        raise ValueError("list length must be a positive integer")
    num_combinations = 2 ** list_length
    combinations = list()
    for i in range(num_combinations):
        # convert i to binary string
        binary_i = "{0:b}".format(i)
        # convert that to binary list
        combination = list(map(int, binary_i))
        # attach missing zeros at head
        combination = [0] * (list_length - len(combination)) + combination
        combinations.append(combination)
    return combinations


# Chunks a list into num_chunks parts of rougly equal size (size differing by at most 1, with the first chunks possibly
# being larger).
def chunk_list(lst, num_chunks):
    len_list = len(lst)
    min_num_entries = math.floor(len_list / num_chunks)
    max_num_entries = math.ceil(len_list / num_chunks)
    num_chunks_w_max_entries = len_list - min_num_entries * num_chunks
    chunks = list()
    last_end = 0
    for i in range(num_chunks):
        start = last_end
        if i < num_chunks_w_max_entries:
            end = start + max_num_entries
        else:
            end = start + min_num_entries
        chunk = lst[start:end]
        chunks.append(chunk)
        last_end = end

    #last_chunk_start = (num_chunks - 1) * num_entries
    #last_chunk = lst[last_chunk_start:]
    #chunks.append(last_chunk)
    return chunks