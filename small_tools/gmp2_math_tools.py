from gmpy2 import log, log10, mpfr
inf = mpfr("inf")

def inf_log10(x):
    if x > 0:
        return log10(x)
    elif x == 0:
        return -inf
    else:
        raise ValueError("math domain error")


def inf_log(x):
    if x > 0:
        return log(x)
    elif x == 0:
        return -inf
    else:
        raise ValueError("math domain error")