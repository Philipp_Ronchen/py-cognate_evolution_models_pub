import itertools


# takes a key and a dictionary with integer values:
# - if key is present in the dictionary, increases the value of key by one
# - if key is not present in the dictionary, adds key:1 to the dictionary
def dict_add_or_increase(dic,key):
    if key in dic:
        dic[key] = dic[key] + 1
    else:
        dic[key] = 1
    new_dic = dic
    return new_dic


# takes a key and a dictionary with sets as values:
# - if key is present in the dictionary, adds the value to the set indexed by key
# - if key is not present in the dictionary, adds key:{value} to the dictionary
def dict_add_to_set(dic, key, value):
    if key in dic:
        dic[key].add(value)
    else:
        dic[key] = {value}
    new_dic = dic
    return new_dic


# changes a dictionary from the format {key:value..} to {value:key..}. If several keys are assigned the same value,
# returns an error
def dict_reverse(dct):
    new_dict = dict()
    for key in dct.keys():
        value = dct[key]
        if value in new_dict:
            raise ValueError("Same value assigned to different keys")
        else:
            new_dict[value] = key
    return new_dict


# gives the cartesian product of a dictionary that has lists as values. It gives a list (or rather a generator)
# of dictionaries that assign to every key one value from the list
# Example: dict_product({"number": [1,2,3], "color": ["orange","blue"] }) is
# [ {"number": 1, "color": "orange"},
#   {"number": 1, "color": "blue"},
#   {"number": 2, "color": "orange"},
#   {"number": 2, "color": "blue"},
#   {"number": 3, "color": "orange"},
#   {"number": 3, "color": "blue"}
# ]
def dict_product(kwargs):
    keys = kwargs.keys()
    vals = kwargs.values()
    for instance in itertools.product(*vals):
        yield dict(zip(keys, instance))


# gives the union of a list of dictionaries. The dictionaries should be disjoint.
def dict_union(dict_list):
    return dict(i for dct in dict_list for i in dct.items())


# given a list of dictionaries, returns a list with duplicates removed
def unique_dicts(dict_list):
    unique_dict_list = [dict(s) for s in set(frozenset(myObject.items()) for myObject in dict_list)]
    return unique_dict_list