from trees import tree_tools
from small_tools import math_tools
from Bio import Phylo


# print functions used both to print given cognate data and the outcomes of the generate_multistate_data function
def print_cog_data(cog_data):
    for m in sorted(cog_data):
        print("### Meaning " + str(m) + " ###")
        for node in sorted(cog_data[m]):
            print("'" + str(node) + "': " + str(cog_data[m][node]) + " ", end = '')
        print("")


# print function used to print the result of the extension operation applied to one-meaning data
def print_extend_data_result(extend_data_result):
    if not extend_data_result[0]:
        print("Contradictory data. Nodes which are determined to have multiple values are:")
        for node in sorted(extend_data_result[1]):
            print("'" + str(node) + "': " + str(extend_data_result[1][node]) + " ", end = '')
    if extend_data_result[0]:
        print("Data is extendable. Extended data:")
        for node in sorted(extend_data_result[1]):
            print("'" + str(node) + "': " + str(extend_data_result[1][node]) + " ", end = '')
    print("")


# print function used to print log likelihoods
def print_lhs(total_log_lh,meaning_lhs):
    print("### Total log10 likelihood ###")
    print(total_log_lh)
    print("### Log10 likelihoods per meaning ###")
    for m in meaning_lhs:
        print(str(m) + ": " + str(math_tools.inf_log10(meaning_lhs[m])) + " ", end='')
    print("")


def draw_tree(tree):
    Phylo.draw(make_phylo_tree(tree))


# takes a tree in the [nodes,edges,root,leaves,internals,parents,children] format and translates it into a PhyloXML tree
def make_phylo_tree(tree):
    # convert edge lengths to floats
    tree = tree_tools.get_float_tree(tree)

    # get nodes, edges, root and children
    nodes = tree[0]
    edges = tree[1]
    root = tree[2]
    children = tree[6]

    clade_dic = {node:Phylo.BaseTree.Clade(name=node) for node in nodes}
    for node in children:
        # filter our edges to children of node
        child_edges = [(node,child,edges[(node,child)]) for child in children[node]]
        # append clades of children
        for (parent,child,length) in child_edges:
            current_clade = clade_dic[node]
            child_clade = clade_dic[child]
            child_clade.branch_length = length
            current_clade.clades.append(child_clade)

    root_clade = clade_dic[root]
    root_clade.date = 0
    phylo_tree = Phylo.PhyloXML.Clade.to_phylogeny(root_clade)
    return phylo_tree