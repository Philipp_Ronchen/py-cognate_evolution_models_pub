from lh_calculation import gmp2_lh_multistate_recursive as lhmr
from lh_calculation import lh_stochastic_dollo as lhsd
from lh_calculation import lh_binary_ctmc as lhb
from small_tools import dict_tools

# gmp2 version of accum_functions


# defines an accumulation function which simultaneously executes  all the recursive calculation steps for all meanings
# in the multistate model
def multistate_accum_func(node, child_values, edges, multistate_data):
    meanings = multistate_data.keys()
    children = child_values.keys()
    new_multistate_value = dict()
    for meaning in meanings:
        meaning_data = multistate_data[meaning]
        change_rate = meaning_data[0]
        is_extendable = meaning_data[1]
        extended_data = meaning_data[2]
        meaning_child_values = {child:child_values[child][meaning] for child in children}
        [change_probs, ret_probs] = lhmr.get_change_ret_probs(edges, change_rate)
        if not is_extendable:
            new_multistate_value[meaning] = [0,0]
        else:
            new_multistate_value[meaning] = \
                lhmr.calculation_step(node,meaning_child_values,change_probs,ret_probs,extended_data)
    return new_multistate_value


# defines an accumulation function which simultaneously executes  all the recursive calculation steps for all meanings
# in the multistate model, given for every meaning a (non-empty) list of meaning_data sets
def multiple_data_multistate_accum_func(node, child_values, edges, multiple_multistate_data):
    meanings = multiple_multistate_data.keys()
    children = child_values.keys()
    new_multistate_value = dict()
    for meaning in meanings:
        meaning_data_sets = multiple_multistate_data[meaning]
        num_meaning_data_sets = len(meaning_data_sets)
        new_multistate_value[meaning] = [None] * num_meaning_data_sets
        for i in range(num_meaning_data_sets):
            meaning_data = meaning_data_sets[i]
            change_rate = meaning_data[0]
            is_extendable = meaning_data[1]
            extended_data = meaning_data[2]
            meaning_child_values = {child:child_values[child][meaning][i] for child in children}
            [change_probs, ret_probs] = lhmr.get_change_ret_probs(edges, change_rate)
            if not is_extendable:
                new_multistate_value[meaning][i] = [0,0]
            else:
                new_multistate_value[meaning][i] = \
                    lhmr.calculation_step(node,meaning_child_values,change_probs,ret_probs,extended_data)
    return new_multistate_value


# defines an accumulation function which simultaneously executes  all the recursive calculation steps for all meanings
# in the multistate model with throw outs
def multistate_w_throw_outs_accum_func(node, child_values, edges, multistate_w_trow_outs_data):
    meanings = multistate_w_trow_outs_data.keys()
    children = child_values.keys()
    new_multistate_value = dict()
    for meaning in meanings:
        meaning_data = multistate_w_trow_outs_data[meaning]
        change_rate = meaning_data[0]
        throw_out_combs_data = meaning_data[1]
        meaning_child_values = {child:child_values[child][meaning] for child in children}
        [change_probs, ret_probs] = lhmr.get_change_ret_probs(edges, change_rate)
        new_multistate_value[meaning] = dict()
        for throw_out_comb in throw_out_combs_data.keys():
            comb_data = throw_out_combs_data[throw_out_comb]
            is_extendable = comb_data[0]
            extended_data = comb_data[1]
            throw_out_comb_child_values = {child:meaning_child_values[child][throw_out_comb] for child in children}
            if not is_extendable:
                new_multistate_value[meaning][throw_out_comb] = [0,0]
            else:
                new_multistate_value[meaning][throw_out_comb] = \
                    lhmr.calculation_step(node,throw_out_comb_child_values,change_probs,ret_probs,extended_data)
    return new_multistate_value


# defines an accumulation function which simultaneously executes  all recursive calculations for the stochastic dollo
# model
def sd_accum_func(node, child_values, edges, sd_data):
    [b, d, patterns] = sd_data
    children = child_values.keys()
    child_death_probs = {child:child_values[child][0] for child in children}
    child_nodes_to_patterns_probs = {child:child_values[child][1] for child in children}
    below_child_subtree_nodes = {child:child_values[child][2] for child in children}
    below_child_edge_survival_probs = {child:child_values[child][3] for child in children}
    below_child_death_probs = {child: child_values[child][4] for child in children}
    below_child_nodes_to_patterns_probs = {child: child_values[child][5] for child in children}

    # subtree nodes
    combined_subtree_nodes = set().union(*below_child_subtree_nodes.values()).union({node})
    # edge survival probs
    old_edge_survival_probs = dict_tools.dict_union(below_child_edge_survival_probs.values())
    new_edge_survival_probs = lhsd.get_edge_survival_probs(edges, d)
    combined_edge_survival_probs = {**old_edge_survival_probs, **new_edge_survival_probs}
    # death probs
    old_death_probs = dict_tools.dict_union(below_child_death_probs.values())
    new_death_prob = lhsd.death_probs_calculation_step(node, combined_edge_survival_probs, child_death_probs)
    combined_death_probs = old_death_probs
    combined_death_probs[node] = new_death_prob
    # node_to_patterns_probs
    new_node_to_patterns_probs = dict()
    combined_nodes_to_patterns_probs = dict()
    for pattern in patterns:
        child_nodes_to_this_pattern_probs = {child:child_nodes_to_patterns_probs[child][pattern] for child in children}
        new_node_to_this_pattern_prob = \
            lhsd.node_to_pattern_prob_calculation_step(node, below_child_subtree_nodes, new_edge_survival_probs,
                                                       child_death_probs, child_nodes_to_this_pattern_probs, pattern)
        new_node_to_patterns_probs[pattern] = new_node_to_this_pattern_prob
        below_child_nodes_to_this_pattern_probs = \
            {child:below_child_nodes_to_patterns_probs[child][pattern] for child in children}
        old_nodes_to_this_pattern_probs = dict_tools.dict_union( below_child_nodes_to_this_pattern_probs.values())
        combined_nodes_to_patterns_probs[pattern] = old_nodes_to_this_pattern_probs
        combined_nodes_to_patterns_probs[pattern][node] = new_node_to_this_pattern_prob
    return [new_death_prob, new_node_to_patterns_probs, combined_subtree_nodes, combined_edge_survival_probs,
            combined_death_probs, combined_nodes_to_patterns_probs]


# defines an accumulation function which simultaneously executes  all recursive calculations for the meaningwise
# stochastic dollo model
def sdm_accum_func(node, child_values, edges, sdm_data):
    meanings = sdm_data.keys()
    children = child_values.keys()
    new_sdm_value = dict()
    for meaning in meanings:
        sd_meaning_data = sdm_data[meaning]
        meaning_child_values = {child:child_values[child][meaning] for child in children}
        new_sdm_value[meaning] = sd_accum_func(node, meaning_child_values, edges, sd_meaning_data)
    return new_sdm_value


# defines an accumlulation function that carries out the recursion step for the binary ctmc model, calculating the
# conditional likelihoods for a node
def binary_ctmc_accum_func(node, child_parallel_clhs, child_edges, ctmc_data):
    [b, d, binary_data] = ctmc_data
    children = child_parallel_clhs.keys()
    num_parallel_values = len(list(child_parallel_clhs.values())[0])
    node_parallel_clhs = list()
    for i in range(num_parallel_values):
        child_clhs = {child:child_parallel_clhs[child][i] for child in children}
        node_clhs = lhb.calculation_step(child_edges, child_clhs, b, d)
        node_parallel_clhs.append(node_clhs)
    return node_parallel_clhs


# for a leaf node, computes the initial values necessary for the multistate recursions
def multistate_leaf_value_function(node, multistate_data):
    meanings = multistate_data.keys()
    multistate_value = dict()
    for meaning in meanings:
        meaning_data = multistate_data[meaning]
        is_extendable = meaning_data[1]
        extended_data = meaning_data[2]
        if not is_extendable:
            multistate_value[meaning] = [0,0]
        else:
            if node in extended_data:
                multistate_value[meaning] = [1,1]
            else:
                multistate_value[meaning] = [1,0]
    return multistate_value


# for a leaf node, computes the initial values necessary for the multistate recursions, given for every meaning a
# (non-empty) list of meaning_data sets
def multiple_data_multistate_leaf_value_function(node, multiple_multistate_data):
    meanings = multiple_multistate_data.keys()
    multistate_value = dict()
    for meaning in meanings:
        meaning_data_sets = multiple_multistate_data[meaning]
        num_meaning_data_sets = len(meaning_data_sets)
        multistate_value[meaning] = [None] * num_meaning_data_sets
        for i in range(num_meaning_data_sets):
            meaning_data = meaning_data_sets[i]
            is_extendable = meaning_data[1]
            extended_data = meaning_data[2]
            if not is_extendable:
                multistate_value[meaning][i] = [0, 0]
            else:
                if node in extended_data:
                    multistate_value[meaning][i] = [1, 1]
                else:
                    multistate_value[meaning][i] = [1, 0]
    return multistate_value


# for a leaf node, computes the initial values necessary for the multistate recursions, given data with throw outs
def multistate_w_throw_outs_leaf_value_function(node, multistate_w_throw_outs_data):
    meanings = multistate_w_throw_outs_data.keys()
    multistate_value = dict()
    for meaning in meanings:
        meaning_data = multistate_w_throw_outs_data[meaning]
        throw_out_combs_data = meaning_data[1]
        multistate_value[meaning] = dict()
        for throw_out_comb in throw_out_combs_data.keys():
            comb_data = throw_out_combs_data[throw_out_comb]
            is_extendable = comb_data[0]
            extended_data = comb_data[1]
            if not is_extendable:
                multistate_value[meaning][throw_out_comb] = [0,0]
            else:
                if node in extended_data:
                    multistate_value[meaning][throw_out_comb] = [1,1]
                else:
                    multistate_value[meaning][throw_out_comb] = [1,0]
    return multistate_value


# for a leaf node, computes the initial values necessary for the stochastic dollo recursions
def sd_leaf_value_function(node, sd_data):
    [b, d, patterns] = sd_data
    new_death_prob = 0
    new_node_to_patterns_probs = dict()
    for pattern in patterns:
        if node in pattern:
            new_node_to_patterns_probs[pattern] = 1
        else:
            new_node_to_patterns_probs[pattern] = 0
    combined_subtree_nodes = {node}
    combined_edge_survival_probs = dict()
    combined_death_probs = {node:new_death_prob}
    combined_nodes_to_patterns_probs = dict()
    for pattern in patterns:
        combined_nodes_to_patterns_probs[pattern] = {node:new_node_to_patterns_probs[pattern]}
    return [new_death_prob, new_node_to_patterns_probs, combined_subtree_nodes, combined_edge_survival_probs,
            combined_death_probs, combined_nodes_to_patterns_probs]


# for a leaf node, computes the initial values necessary for the meaningwise stochastic dollo recursions
def sdm_leaf_value_function(node, sdm_data):
    meanings = sdm_data.keys()
    value = dict()
    for meaning in meanings:
        sd_meaning_data = sdm_data[meaning]
        value[meaning] = sd_leaf_value_function(node, sd_meaning_data)
    return value


# for a leaf node, computes the initial values necessary for the binary ctmc recursions
def binary_ctmc_leaf_value_function(node, ctmc_data):
    [b, d, binary_data] = ctmc_data
    leaf_parallel_clhs = list()
    node_data = binary_data[node]
    for data_point in node_data:
        if data_point == 0:
            leaf_parallel_clhs.append({0:1.0, 1:0.0})
        elif data_point == 1:
            leaf_parallel_clhs.append({0:0.0, 1:1.0})
    return leaf_parallel_clhs



# trivial functions for testing purposes
def trivial_leaf_value_function(node, data):
    return None


def trivial_accum_function(node, child_values, edges, data):
    return None