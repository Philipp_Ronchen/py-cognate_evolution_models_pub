### contains functions that perform transformations and calculations of families of trees with accumulated information
from lh_calculation import lh_stochastic_dollo as lhsd
from lh_calculation import lh_binary_ctmc as lhb
from lh_calculation import binary_ctmc_condition_outcomes as b_cond_outcome
from small_tools import math_tools
from trees import tree_tools
import copy


# takes as input a dictionary of accum_trees which are of the form {node:{age:[tree_edges, value]...}...}
# Returns a list of value_trees of the form [tree_edges, node_ages, value], throwing out all partial trees that do not
# include the root
def accum_to_value_trees(accum_trees, root):
    root_accum_trees = accum_trees[root]
    value_trees = list()
    for age in root_accum_trees.keys():
        for accum_tree in root_accum_trees[age]:
            tree_edges = accum_tree[0]
            value = accum_tree[1]
            full_tree = tree_tools.get_tree_from_edges(tree_edges)
            node_ages = tree_tools.get_node_ages(full_tree)
            value_trees.append([tree_edges, node_ages, value])
    return value_trees


def filter_trees_by_node_age(tree_list, node, node_age):
    filtered_list = [value_tree for value_tree in tree_list if value_tree[1][node] == node_age]
    return filtered_list


def get_tree_with_max_value(value_trees):
    if not value_trees:
        raise ValueError("input list is empty")
    else:
        current_best_tree = value_trees[0]
        current_max = current_best_tree[2]
        for value_tree in value_trees:
            value = value_tree[2]
            if value > current_max:
                current_best_tree = value_tree
                current_max = value
    return current_best_tree


def set_value(value_tree, new_value):
    value_tree[2] = new_value
    return value_tree


def sd_get_trees_with_intensities(tree_topology, value_trees, b, d, patterns):
    intensity_trees = list()
    for value_tree in value_trees:
        value_tree = copy.deepcopy(value_tree)
        tree_edges = value_tree[0]
        tree = copy.deepcopy(tree_topology)
        tree[1] = tree_edges
        value = value_tree[2]
        edge_survival_probs = value[3]
        death_probs = value[4]
        nodes_to_patterns_probs = value[5]
        intensities = \
            lhsd.get_intensities(tree,b,d,edge_survival_probs,death_probs, nodes_to_patterns_probs, patterns)
        set_value(value_tree, intensities)
        intensity_trees.append(value_tree)
    return intensity_trees


def sd_get_log_lh_trees(trees_with_intensities):
    lh_trees = list()
    for value_tree in trees_with_intensities:
        value_tree = copy.deepcopy(value_tree)
        value = value_tree[2]
        registered_birth_intensity = value[0]
        pattern_intensities = value[1]
        log_lh = lhsd.get_log_lh_from_intensities(registered_birth_intensity,pattern_intensities)
        set_value(value_tree, log_lh)
        lh_trees.append(value_tree)
    return lh_trees


def sd_accum_to_log_lh_trees(tree_topology, accum_trees, b, d, patterns):
    root = tree_topology[2]
    value_trees = accum_to_value_trees(accum_trees, root)
    trees_with_intensities =  sd_get_trees_with_intensities(tree_topology,value_trees,b,d,patterns)
    log_lh_trees = sd_get_log_lh_trees(trees_with_intensities)
    return log_lh_trees


def sdm_get_trees_with_intensities(tree_topology, value_trees, sdm_data):
    meanings = sdm_data.keys()
    intensity_trees = list()
    for value_tree in value_trees:
        value_tree = copy.deepcopy(value_tree)
        tree_edges = value_tree[0]
        tree = copy.deepcopy(tree_topology)
        tree[1] = tree_edges
        value = value_tree[2]
        intensities = dict()
        for meaning in meanings:
            [b, d, patterns] = sdm_data[meaning]
            meaning_value = value[meaning]
            edge_survival_probs = meaning_value[3]
            death_probs = meaning_value[4]
            nodes_to_patterns_probs = meaning_value[5]
            intensities[meaning] = \
                lhsd.get_intensities(tree,b,d,edge_survival_probs,death_probs, nodes_to_patterns_probs, patterns)
        set_value(value_tree, intensities)
        intensity_trees.append(value_tree)
    return intensity_trees


def sdm_get_meaning_lh_trees(trees_with_intensities):
    meaning_lh_trees = list()
    for value_tree in trees_with_intensities:
        value_tree = copy.deepcopy(value_tree)
        value = value_tree[2]
        meanings = value.keys()
        meaning_lhs = dict()
        for meaning in meanings:
            meaning_value = value[meaning]
            registered_birth_intensity = meaning_value[0]
            pattern_intensities = meaning_value[1]
            meaning_lhs[meaning] = lhsd.get_lh_from_intensities(registered_birth_intensity, pattern_intensities)
        set_value(value_tree, meaning_lhs)
        meaning_lh_trees.append(value_tree)
    return meaning_lh_trees


def sdm_get_meaning_log_lh_trees(trees_with_intensities):
    meaning_log_lh_trees = list()
    for value_tree in trees_with_intensities:
        value_tree = copy.deepcopy(value_tree)
        value = value_tree[2]
        meanings = value.keys()
        meaning_log_lhs = dict()
        for meaning in meanings:
            meaning_value = value[meaning]
            registered_birth_intensity = meaning_value[0]
            pattern_intensities = meaning_value[1]
            meaning_log_lhs[meaning] = lhsd.get_log_lh_from_intensities(registered_birth_intensity, pattern_intensities)
        set_value(value_tree, meaning_log_lhs)
        meaning_log_lh_trees.append(value_tree)
    return meaning_log_lh_trees


def sdm_get_log_lh_trees(meaning_log_lh_trees):
    lh_trees = list()
    for value_tree in meaning_log_lh_trees:
        value_tree = copy.deepcopy(value_tree)
        value = value_tree[2]
        log_lh = sum(value.values())
        set_value(value_tree, log_lh)
        lh_trees.append(value_tree)
    return lh_trees


def sdm_accum_to_meaning_lh_trees(tree_topology, accum_trees, sdm_data):
    root = tree_topology[2]
    value_trees = accum_to_value_trees(accum_trees, root)
    trees_with_intensities = sdm_get_trees_with_intensities(tree_topology, value_trees, sdm_data)
    meaning_lh_trees = sdm_get_meaning_lh_trees(trees_with_intensities)
    return meaning_lh_trees


def sdm_accum_to_log_lh_trees(tree_topology, accum_trees, sdm_data):
    root = tree_topology[2]
    value_trees = accum_to_value_trees(accum_trees, root)
    trees_with_intensities = sdm_get_trees_with_intensities(tree_topology, value_trees, sdm_data)
    meaning_log_lh_trees = sdm_get_meaning_log_lh_trees(trees_with_intensities)
    log_lh_trees = sdm_get_log_lh_trees(meaning_log_lh_trees)
    return log_lh_trees


def multistate_get_log_lh_trees(value_trees):
    lh_trees = list()
    for value_tree in value_trees:
        value_tree = copy.deepcopy(value_tree)
        value = value_tree[2]
        meaning_lhs = map(lambda x: x[0], value.values())
        meaning_log_lhs = map(math_tools.inf_log, meaning_lhs)
        log_lh = sum(meaning_log_lhs)
        set_value(value_tree, log_lh)
        lh_trees.append(value_tree)
    return lh_trees


def multistate_accum_to_log_lh_trees(accum_trees,root):
    value_trees = accum_to_value_trees(accum_trees,root)
    log_lh_trees = multistate_get_log_lh_trees(value_trees)
    return log_lh_trees


# given value trees which as a value assign to every meaning and every combination of thrown out nodes a likelihood,
# returns a value_tree that as a value assigns to every meaning the combination of nodes that when thrown out maximimes
# the likelihood, and the likelihood for the meaning when the nodes are thrown out
def multistate_get_best_throw_out_meaning_lh_trees(value_trees):
    new_trees = list()
    for value_tree in value_trees:
        value_tree = copy.deepcopy(value_tree)
        value = value_tree[2]
        meanings = value.keys()
        new_value = dict()
        for meaning in meanings:
            throw_out_combs = value[meaning].keys()
            best_throw_out_comb = max(throw_out_combs, key=lambda x:value[meaning][x][0])
            best_throw_out_comb_lh = value[meaning][best_throw_out_comb][0]
            new_value[meaning] = [best_throw_out_comb, best_throw_out_comb_lh]
        set_value(value_tree, new_value)
        new_trees.append(value_tree)
    return new_trees


def binary_ctmc_get_log_lh_trees(value_trees, root_freqs):
    lh_trees = list()
    for value_tree in value_trees:
        value_tree = copy.deepcopy(value_tree)
        value = value_tree[2]
        parallel_lhs = map(lambda x: lhb.lh_from_root_clhs(x,root_freqs), value)
        parallel_log_lhs = map(math_tools.inf_log, parallel_lhs)
        log_lh = sum(parallel_log_lhs)
        set_value(value_tree, log_lh)
        lh_trees.append(value_tree)
    return lh_trees


def binary_ctmc_accum_to_log_lh_trees(accum_trees, root, root_freqs):
    value_trees = accum_to_value_trees(accum_trees,root)
    log_lh_trees = binary_ctmc_get_log_lh_trees(value_trees, root_freqs)
    return log_lh_trees


def binary_ctmc_adjust_log_lh_trees_for_0_data(log_lh_trees, topology, b, d, f, data_nodes, num_parallel_values):
    adjusted_trees = list()
    for log_lh_tree in log_lh_trees:
        log_lh_tree = copy.deepcopy(log_lh_tree)
        edges = log_lh_tree[0]
        log_lh = log_lh_tree[2]
        tree = copy.deepcopy(topology)
        tree[1] = edges
        adjusted_log_lh = b_cond_outcome.adjust_log_lh_for_0_data(log_lh, tree, b, d, f, data_nodes, num_parallel_values)
        set_value(log_lh_tree, adjusted_log_lh)
        adjusted_trees.append(log_lh_tree)
    return adjusted_trees


def binary_ctmc_accum_to_adjusted_log_lh_trees(accum_trees, topology, b, d, f, data_nodes, num_parallel_values):
    root = topology[2]
    value_trees = accum_to_value_trees(accum_trees,root)
    log_lh_trees = binary_ctmc_get_log_lh_trees(value_trees, f)
    adjusted_log_lh_trees = binary_ctmc_adjust_log_lh_trees_for_0_data(log_lh_trees, topology, b, d, f, data_nodes,
                                                                       num_parallel_values)
    return adjusted_log_lh_trees