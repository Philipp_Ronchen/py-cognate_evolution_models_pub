from branch_length_tree_operations import build_trees_and_accum as bta
from branch_length_tree_operations import accum_functions as af
from branch_length_tree_operations import accum_results_transf as art
from lh_calculation import lh_binary_ctmc as lhb
import math

# gives the tree with maximal value in a non-empty list of value_trees
def get_max_value_tree(value_trees):
    max_value_tree = max(value_trees, key=lambda x:x[2])
    return(max_value_tree)


# the maximum likelihood tree and the combination (b,d) of paramters in blist and dlist that maximise the likelihood over
# all trees compatible with the age_constraints, with respect to the Stochastic Dollo model
def sd_max_lh_free_parameters(tree_topology, blist, dlist, max_tree_height, age_constraints, age_resolution, patterns):
    best_trees = sd_parameter_combinations_max_lhs(tree_topology, blist, dlist, max_tree_height, age_constraints, age_resolution, patterns)
    best_comb = max(best_trees.items(), key=lambda x:x[1][2])
    return best_comb


# gives for all combinations of (b,d) of paramters in blist and dlist the tree that maximises the likelihood over
# all trees compatible with the age_constraints, with respect to the Stochastic Dollo model
def sd_parameter_combinations_max_lhs(tree_topology, blist, dlist, max_tree_height, age_constraints, age_resolution, patterns):
    best_trees = dict()
    for b in blist:
        for d in dlist:
            sd_data = [b,d,patterns]
            accum_trees = bta.build_trees_and_accumulate(tree_topology, max_tree_height, age_constraints, age_resolution, sd_data,
                                           af.sd_leaf_value_function, af.sd_accum_func)
            log_lh_trees = art.sd_accum_to_log_lh_trees(tree_topology, accum_trees, b, d, patterns)
            best_curr_tree = get_max_value_tree(log_lh_trees)
            best_trees[(b,d)] = best_curr_tree
    return best_trees


# the maximum likelihood tree and the combination (b,d, root_freqs) of paramters in blist and dlist and root_freq_list, that maximise the likelihood over
# all trees compatible with the age_constraints, with respect to the binary CTMC model
def binary_ctmc_max_lh_free_parameters(tree_topology, blist, dlist, root_freq_list, max_tree_height, age_constraints, age_resolution,
                                      binary_data, adjust_for_0_data = False):
    best_trees = binary_ctmc_parameter_combinations_max_lhs(tree_topology, blist, dlist, root_freq_list, max_tree_height,
                                                            age_constraints, age_resolution, binary_data,
                                                            adjust_for_0_data)
    best_comb = max(best_trees.items(), key=lambda x:x[1][2])
    return best_comb


# gives for all combinations of (b,d) of paramters in blist and dlist and root_freq_list the tree that maximises the likelihood over
# all trees compatible with the age_constraints, with respect to the binary CTMC model
def binary_ctmc_parameter_combinations_max_lhs(tree_topology, blist, dlist, root_freq_list, max_tree_height,
                                               age_constraints, age_resolution, binary_data, adjust_for_0_data = False):
    root = tree_topology[2]
    data_nodes = binary_data.keys()
    num_parallel_values = lhb.get_num_parallel_values(binary_data)
    best_trees = dict()
    for b in blist:
        for d in dlist:
            ctmc_data = [b,d,binary_data]
            accum_trees = bta.build_trees_and_accumulate(tree_topology, max_tree_height, age_constraints,
                                                         age_resolution, ctmc_data, af.binary_ctmc_leaf_value_function,
                                                         af.binary_ctmc_accum_func)
            for root_freqs in root_freq_list:
                if adjust_for_0_data == True:
                    log_lh_trees = art.binary_ctmc_accum_to_adjusted_log_lh_trees(accum_trees, tree_topology, b, d,
                                                                                  root_freqs, data_nodes,
                                                                                  num_parallel_values)
                else:
                    log_lh_trees = art.binary_ctmc_accum_to_log_lh_trees(accum_trees, root, root_freqs)
                best_curr_tree = get_max_value_tree(log_lh_trees)
                best_trees[(b,d,root_freqs)] = best_curr_tree
    return best_trees

