from trees import tree_tools


# returns an age constraint that is truncated such that it lies within super_constraint
def restrict_constraint(old_age_constraint, super_constraint):
    if super_constraint is None:
        return old_age_constraint
    else:
        if old_age_constraint is None:
            return super_constraint
        else:
            new_age_constraint = [None] * 2
            old_age_constraint[0] = max(old_age_constraint[0], super_constraint[0])
            old_age_constraint[1] = min(old_age_constraint[1], super_constraint[1])
            return new_age_constraint


# extends the given age_constraints by calculating the implications that the age constraint of a certain node has on the
# age constraints of other nodes in tree
def propagate_constraints_through_tree(tree, age_constraints, max_tree_height):
    nodes = tree[0]
    root = tree[2]
    leaves = tree[3]
    parents = tree[5]
    children = tree[6]

    for node in nodes:
        if node not in age_constraints:
            age_constraints[node] = [0,max_tree_height]
    age_constraints[root][1] = min(age_constraints[root][1], max_tree_height)

    if root in children:
        current_level = children[root]
    else:
        current_level = set()

    # first iteration, from the root downward
    while current_level:
        for node in current_level:
            parent = parents[node]
            age_constraints[node][1] = min(age_constraints[node][1], age_constraints[parent][1])
        current_level = tree_tools.get_node_set_children(tree, current_level)

    # second iteration, from the leaves upward
    current_level = leaves
    calculated = set()
    while current_level:
        for node in current_level:
            if node in children:
                age_constraints[node][0] = \
                    max({age_constraints[child][0] for child in children[node]}.union({age_constraints[node][0]}))
        calculated = calculated.union(current_level)
        current_level = tree_tools.get_new_node_set_parents(tree, calculated)

    for node in nodes:
        if not age_constraints[node][0] <= age_constraints[node][1]:
            raise ValueError("Incompatible age constraints")

    return age_constraints


# gets a range of ages that lie within age_constraint. The "resolution" of the age range is given is given by increment
# that specifies the minimal difference between two ages
def get_age_range(age_constraint,increment):
    if age_constraint[0] % increment == 0:
        start_value = age_constraint[0]
    else:
        start_value = age_constraint[0]# - age_constraint[0] % increment + increment
    age_range = range(start_value, age_constraint[1] + 1, increment)
    return age_range

