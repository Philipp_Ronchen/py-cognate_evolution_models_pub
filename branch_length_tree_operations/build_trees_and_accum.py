from typing import Dict, List, Set
from trees import tree_tools
from small_tools import dict_tools
import gc

from branch_length_tree_operations import age_constraint_tools as act


def build_trees_and_accumulate(tree, max_tree_height, age_constraints, age_resolution, data, leaf_value_function,
                               accum_function):
    nodes = tree[0]
    root = tree[2]
    children = tree[6]

    age_constraints = act.propagate_constraints_through_tree(tree, age_constraints, max_tree_height)
    age_ranges = {node: act.get_age_range(age_constraints[node], age_resolution) for node in nodes}

    accum_trees = dict()
    for node in nodes:
        accum_trees[node] = {age: [] for age in age_ranges[node]}

    levels = tree_tools.get_levels(tree)

    for leaf in levels[0]:
        for age in age_ranges[leaf]:
            accum_trees[leaf][age].append((dict(), leaf_value_function(leaf, data)))

    i = 0
    for level in levels[1:]:
        for node in level:
            node_children = children[node]
            for node_age in age_ranges[node]:
                gc.collect()
                child_age_combs = get_node_age_combs(node_children, age_ranges, node_age)
                for child_age_comb in child_age_combs:
                    new_edges = new_edges_from_child_age_comb(node, node_age, child_age_comb)
                    accum_tree_combs = get_accum_tree_combs(child_age_comb,accum_trees)
                    for accum_tree_comb in accum_tree_combs:
                        tree_comb = {child:accum_tree_comb[child][0] for child in node_children}
                        accum_comb = {child:accum_tree_comb[child][1] for child in node_children}
                        comb_edges = dict_tools.dict_union(tree_comb.values())
                        new_tree = {**comb_edges, **new_edges}
                        new_value = accum_function(node, accum_comb, new_edges, data)
                        accum_trees[node][node_age].append((new_tree, new_value))
                        if node == root:
                            #print(i)
                            i += 1
    return accum_trees


# takes a dictionary node_ages that assigns to each nodes in the dictionary a list of possible ages. Returns all the
# combinations for which the ages are small than max_node_age, as a list of dictionaries
def get_node_age_combs(children: Set[str], node_ages: Dict[str,List[int]], max_node_age : int) -> List[Dict[str,int]]:
    truncated_node_ages = dict()
    for child in children:
        truncated_node_ages[child] = [age for age in node_ages[child] if age <= max_node_age]
    children_node_ages = {child:truncated_node_ages[child] for child in children}
    node_age_combs = list(dict_tools.dict_product(children_node_ages))
    return node_age_combs


# gives all the combination of accum trees (pairs of trees represented by edge sets and data values) that belong to
# nodes specified in age_combs with the ages specfied in age_combs
def get_accum_tree_combs(age_comb, accum_trees):
    nodes = age_comb.keys()
    accum_trees_w_ages = {node:accum_trees[node][age_comb[node]]for node in nodes}
    accum_tree_combs = list(dict_tools.dict_product(accum_trees_w_ages))
    return accum_tree_combs


# given a node and a node age, and a dictionary of ages for the children of the node, gives all the edges that are
# necessary to add if the tree is to be extended with node
def new_edges_from_child_age_comb(node, node_age, child_age_comb):
    children = child_age_comb.keys()
    new_edges = dict()
    for child in children:
        new_edges[(node, child)] = node_age - child_age_comb[child]
    return new_edges


# def get_age_combs_and_lhs(tree, max_tree_height, age_constraints, age_resolution, one_meaning_data, c):
#     nodes = tree[0]
#     leaves = tree[3]
#     children = tree[6]
#
#     age_constraints = act.propagate_constraints_through_tree(tree, age_constraints, max_tree_height)
#     age_ranges = {node: act.get_age_range(age_constraints[node], age_resolution) for node in nodes}
#
#     # extending the data
#     one_meaning_data = lhmr.extend_data(tree, one_meaning_data)[1]
#
#     results = dict()
#     for node in nodes:
#         results[node] = {age:[] for age in age_ranges[node]}
#
#     levels = tree_tools.get_levels(tree)
#
#     for leaf in leaves:
#         p = 1
#         if leaf in one_meaning_data:
#             q = 1
#         else:
#             q = 0
#         for age in age_ranges[leaf]:
#             results[leaf][age].append((dict(), p, q))
#
#     for level in levels[1:]:
#         for node in level:
#             child_ages = {child: results[child].keys() for child in children[node]}
#             child_age_combs = list(dict_tools.dict_product(**child_ages))
#             for child_age_comb in child_age_combs:
#                 max_child_age = max(child_age_comb.values())
#                 for age in [age for age in age_ranges[node] if age >= max_child_age]:
#                     children_results = {child:results[child][child_age_comb[child]] for child in children[node]}
#                     result_combs = list(dict_tools.dict_product(**children_results))
#                     for result_comb in result_combs:
#                         subtree_comb = {child:result_comb[child][0] for child in children[node]}
#                         p_comb = {child:result_comb[child][1] for child in children[node]}
#                         q_comb = {child:result_comb[child][2] for child in children[node]}
#                         children_lh_results = {child:(p_comb[child],q_comb[child]) for child in children[node]}
#                         child_edges = {(node,child):(age - child_age_comb[child]) for child in children[node]}
#
#                         change_probs = dict()
#                         ret_probs = dict()
#
#                         new_tree = dict(i for dct in subtree_comb.values() for i in dct.items())
#                         for edge in child_edges:
#                             t = child_edges[edge]
#                             new_tree[edge] = t
#                             change_probs[edge] = 1 - math.exp(- c * t)
#                             ret_probs[edge] = 1 - change_probs[edge]
#                         (p,q) = lhmr.calculation_step(node, children_lh_results, change_probs, ret_probs, one_meaning_data)
#                         results[node][age].append((new_tree,p,q))
#     return results
#
#
#
# def get_branch_length_trees(tree, max_tree_height, age_constraints, age_resolution):
#     nodes = tree[0]
#     root = tree[2]
#
#     age_constraints = act.propagate_constraints_through_tree(tree, age_constraints, max_tree_height)
#     age_ranges = {node:act.get_age_range(age_constraints[node], age_resolution) for node in nodes}
#     return branch_length_trees_recursion_step(root, tree, age_ranges)
#
#
# # Gives a dictionary, which to every age in age_ranges[current_node] gives a list of branch length combinations on the
# # tree up to node current_node. Given all the possible tree combinations that agree with age_ranges. A branch length
# # combination is a dictionary that associates to every edge below current_node a dictionary.
# def branch_length_trees_recursion_step(current_node: str, tree: List, age_ranges : Dict[str,List[int]]) \
#         -> Dict[int,List[Dict[str,int]]]:
#     leaves = tree[3]
#     children = tree[6]
#
#     tree_dict = {age:list() for age in age_ranges[current_node]}
#     if current_node in leaves:
#         for age in age_ranges[current_node]:
#             tree_dict[age] = [dict()]
#     else:
#         children_list = list(children[current_node])
#         num_children = len(children_list)
#         child_ages = {child:age_ranges[child] for child in children_list}
#         # for every child gives a dictionary given all the possible branch length combinations
#         children_results = [branch_length_trees_recursion_step(child,tree,age_ranges) for child in children_list]
#         child_age_combinations = list(itertools.product(*child_ages.values()))
#         for child_age_comb in child_age_combinations:
#             max_child_age = max(child_age_comb)
#             for age in [age for age in age_ranges[current_node] if age >= max_child_age]:
#                 child_tree_list = [children_results[i][child_age_comb[i]] for i in range(num_children)]
#                 tree_combinations = list(itertools.product(*child_tree_list))
#                 for tree_comb in tree_combinations:
#                     new_tree = dict(i for dct in tree_comb for i in dct.items())
#                     # add branches of right lengths from current_node to its children
#                     for i in range(num_children):
#                         current_child = children_list[i]
#                         edge_length = age - child_age_comb[i]
#                         new_tree[(current_node,current_child)] = edge_length
#                     tree_dict[age].append(new_tree)
#     return tree_dict
#
#
# def ml_brute_force(tree, c, branch_length_combs, one_meaning_data):
#     root = tree[2]
#     children = tree[6]
#     extended_data = lhmr.extend_data(tree, one_meaning_data)[1]
#     lh_list = list()
#     for branch_length_comb in branch_length_combs:
#         # get change and retention probabilities along all edges
#         change_probs = dict()
#         ret_probs = dict()
#         for edge in branch_length_comb:
#             t = branch_length_comb[edge]
#             change_probs[edge] = 1 - math.exp(-c * t)
#             ret_probs[edge] = math.exp(-c * t)
#
#         lh_list.append(lhmr.recursion_step(root, children, change_probs, ret_probs, extended_data)[0])
#         max_lh = max(lh_list)
#         best_branch_length_index = lh_list.index(max(lh_list))
#         best_branch_length_comb = branch_length_combs[best_branch_length_index]
#         return[best_branch_length_comb,max_lh]





# # returns a dictionary that for every age within an age range for focal_node returns the maximum likelihood tree
# def get_ml_branch_lenghts_multistate(tree, change_rates, cog_data, age_constraints, max_tree_height, age_resolution,
#                                      focal_node):
#     nodes = tree[0]
#     edges = tree[1]
#     root = tree[2]
#     leaves = tree[3]
#     internals = tree[4]
#     parents = tree[5]
#     children = tree[6]
#
#     age_constraints = act.propagate_constraints_through_tree(tree, age_constraints, max_tree_height)
#     age_ranges = {node:act.get_age_range(age_constraints[node], age_resolution) for node in nodes}
#
#     {age:[p,q] for age in age_ranges[node]}
#     for age in age_range[node]:
#
#
#
#     print(age_ranges)