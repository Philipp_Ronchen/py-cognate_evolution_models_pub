from branch_length_tree_operations import build_trees_and_accum as bta
from branch_length_tree_operations import gmp2_accum_functions as af
from branch_length_tree_operations import gmp2_accum_results_transf as art
from frozendict import frozendict

# gmp2 version of meaning_lhs

# Constructs all the trees compatible with tree_topology, max_tree_height, age_constraints and age_resoltuion.
# Outputs a dictionary of the form {node_ages:{meaning:meaning_lh...}...] where node_ages is a frozendict of the form
# {node:age} and meaning_lh is the meaning_lh calculated in accordance to the multistate model.
def multistate_meaningwise(tree_topology, max_tree_height, age_constraints, age_resolution, data):
    root = tree_topology[2]
    accum_trees = bta.build_trees_and_accumulate(tree_topology, max_tree_height, age_constraints, age_resolution, data,
                                                 af.multistate_leaf_value_function,
                                                 af.multistate_accum_func)
    value_trees = art.accum_to_value_trees(accum_trees, root)
    lh_trees = dict()
    for value_tree in value_trees:
        node_ages = frozendict(value_tree[1])
        value = value_tree[2]
        meanings = value.keys()
        meaning_lhs = {m:value[m][0] for m in meanings}
        lh_trees[node_ages] = meaning_lhs
    return lh_trees


# Takes as input data that assigns to every meaning a list of possible data sets [meaning_data_0, meaning_data_1, ...].
# Constructs all the trees compatible with tree_topology, max_tree_height, age_constraints and age_resoltuion.
# Outputs a dictionary of the form {node_ages:{meaning:[meaning_lh_0, meaning_lh_1...]...}...] where node_ages
# is a frozendict of the form {node:age} and the meaning_lhs are calculated in accordance to the multistate model.
def multiple_data_multistate_meaningwise(tree_topology, max_tree_height, age_constraints, age_resolution, data):
    root = tree_topology[2]
    accum_trees = bta.build_trees_and_accumulate(tree_topology, max_tree_height, age_constraints, age_resolution, data,
                                                 af.multiple_data_multistate_leaf_value_function,
                                                 af.multiple_data_multistate_accum_func)
    value_trees = art.accum_to_value_trees(accum_trees, root)
    lh_trees = dict()
    for value_tree in value_trees:
        node_ages = frozendict(value_tree[1])
        value = value_tree[2]
        meanings = value.keys()
        meaning_lhs = dict()
        for m in meanings:
            meaning_lh_list = list(map(lambda x: x[0], value[m]))
            meaning_lhs[m] = meaning_lh_list
        lh_trees[node_ages] = meaning_lhs
    return lh_trees