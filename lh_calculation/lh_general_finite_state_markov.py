from functools import reduce
from operator import mul
from trees import tree_tools
from small_tools import math_tools
# contains functions for calculating likelihoods in some finite state Markov model, where the allowed
# states for different nodes ca differ. Uses the Felsenstein pruning algorithm (Felsenstein 1973)

# calculates the log likelihood for an arbitrary markov model with finitely many states given parallel data, that is for any
# leaf node a list of states is given (of length num_parallel_values)
def calculate_parallel_data_log_lh(topology, states, edge_transition_probs, root_freqs, num_parallel_values, parallel_data):
    log_lh = 0
    for i in range(num_parallel_values):
        current_data = {node: parallel_data[node][i] for node in parallel_data.keys()}
        current_lh = calculate_lh(topology, states, edge_transition_probs, root_freqs, current_data)
        log_lh += math_tools.inf_log(current_lh)
    return log_lh


# calculates the likelihood for an arbitrary markov model with finitely many states. The states that are allowed for
# different nodes in the tree can differ.
# The function takes a tree topology, the possible states for all node (as a dictionary of sets),
# the frequencies for the root_states (as a dictionary of states), edge transition probabilities and data.
# edge transition probabilities are given as a dictionary over all edges. For a certain edge of the form (parent,child),
# edge_transition_probs[cedge] is again a dictionary with all combinations of the parent states and the child states
# as keys.
# data is a dictionary that assigns every leaf node a state
def calculate_lh(topology, states, edge_transition_probs, root_freqs, data):
    root = topology[2]
    children = topology[6]
    levels = tree_tools.get_levels(topology)
    clhs = dict()
    for node in levels[0]:
        node_states = states[node]
        clhs[node] = get_leaf_clhs(node, node_states, data)
    for level in levels[1:]:
        for node in level:
            node_children = children[node]
            node_states = states[node]
            child_clhs = {child:clhs[child] for child in node_children}
            child_states = {child:states[child] for child in node_children}
            child_edges = {(node,child) for child in node_children}
            child_edge_transition_probs = {edge:edge_transition_probs[edge] for edge in child_edges}
            clhs[node] = calculation_step(node_states, child_states, child_clhs, child_edge_transition_probs)
    root_clhs = clhs[root]
    lh = lh_from_root_clhs(root_clhs, root_freqs)
    return lh


def lh_from_root_clhs(root_clhs, root_freqs):
    root_states = root_clhs.keys()
    lh = sum([root_freqs[state] * root_clhs[state] for state in root_states])
    return lh


# calculates the conditional likelihoods for the states of a given node given the possible states of the node, the
# possible states of its children (as a dictionary with the children as keys), the conditional likelihoods for the
# children (also as a dictionary) and the edge transition probabilities (also as a dictionary, with the edges from node
# to its children as keys)
# given a certain child_edge, child_edge_transition_probs[child_edge] is again a dictionary with all combinations of
# child_states and node_states as keys.
def calculation_step(node_states, child_states, child_clhs, child_edge_transition_probs):
    node_clhs = dict()
    edges = child_edge_transition_probs.keys()
    for state in node_states:
        edge_contributions = dict()
        for edge in edges:
            child = edge[1]
            child_state_contributions = dict()
            for child_state in child_states[child]:
                child_state_contributions[child_state] \
                    = child_edge_transition_probs[edge][(state,child_state)]*child_clhs[child][child_state]
            edge_contributions[edge] = sum(child_state_contributions.values())
        node_clhs[state] = reduce(mul, edge_contributions.values(), 1)
    return node_clhs


# calculates the conditional likelihoods for the states of a given leaf node
def get_leaf_clhs(node, node_states, data):
    node_clhs = dict()
    for state in node_states:
        if state == data[node]:
            node_clhs[state] = 1
        else:
            node_clhs[state] = 0
    return node_clhs




