from lh_calculation import lh_multistate_recursive as lhm
from small_tools import dict_tools
from trees import tree_tools


# calculates the extension of set_data. For set data, any data_node is assigned a set of forms. Standard multistate data
# can be viewed as set data where every set has cardinality 0 or 1. Whenever a data_node in the set data or a node in
# extension has a set of forms of cardinality >= 2, the data is "contradictory" with respect to the standard multistate
# model. This function does not explicitly test for contradictions.
def extend_set_data(tree, set_data):
    nodes = tree[0]
    root = tree[2]
    leaves = tree[3]
    parents = tree[5]
    children = tree[6]

    # stores for every node all the data values that occur in the subtree below the node, and assigns every such value
    # its "multiplicity". The multiplicity of the value at the node is the number of children of the node which have
    # the node in their subtree, plus one if the node is determined to have the value directly from the data
    subtree_values = {node:dict() for node in nodes}

    # stores for every node all the subtree_values of its parent which have multiplicity at least 2. These are exactly
    # the values which cannot have been propagated upwards from the node to its parent alone, so they must occur
    # somewhere else in the tree (not only in the subtree of node)
    ancestral_values = {node:set() for node in nodes}

    # stores all nodes which are determined by the extension algorithm and the values the node is determined to have.
    determined_values = dict()

    # first iteration, from the leaves upwards, used to determined subtree_values
    current_level = leaves
    calculated = set()
    while current_level:
        for node in current_level:
            if node in set_data:
                for cog_value in set_data[node]:
                    dict_tools.dict_add_or_increase(subtree_values[node], cog_value)
            if node in children:
                for child in children[node]:
                    for cog_value in subtree_values[child]:
                        dict_tools.dict_add_or_increase(subtree_values[node], cog_value)
        calculated = calculated.union(current_level)
        current_level = tree_tools.get_new_node_set_parents(tree, calculated)

    # second iteration, from the root downwards, used to determined ancestral_values
    ancestral_values[root] = set()
    if root in children:
        current_level = children[root]
    else:
        current_level = set()
    while current_level:
        for node in current_level:
            parent = parents[node]
            ancestral_values[node] = ancestral_values[parent].union(
                {cog_value for cog_value in subtree_values[parent] if subtree_values[parent][cog_value] >= 2})
        current_level = tree_tools.get_node_set_children(tree, current_level)

    # nodes are determined if and only if one of the following points hold:
    # - they are in the (non-extended) data
    # - they have a subtree_value with multiplicity at least 2
    # - they have a subtree_value which is also an ancestral_value
    for node in nodes:
        if node in set_data:
            for cog_value in set_data[node]:
                dict_tools.dict_add_to_set(determined_values, node, cog_value)
        for cog_value in subtree_values[node]:
            if subtree_values[node][cog_value] >= 2 or cog_value in ancestral_values[node]:
                dict_tools.dict_add_to_set(determined_values, node, cog_value)

    return determined_values
