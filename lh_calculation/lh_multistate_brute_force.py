import numpy as np
import math
from small_tools import math_tools
from trees import tree_tools


# calculates the likelihood of a tree given cog_data by brute force, that is by going through every possible combination
# of innovations and retentions and testing whether it is compatible with the data. This has exponential runtime and
# can only be done for very small trees
def compute_lh_bf(tree,c,cog_data):
    nodes = tree[0]
    edges = tree[1]
    root = tree[2]
    parents = tree[5]

    # get meanings in the data
    meanings = set(cog_data.keys())

    # calculate number of possible combinations of innovations and retentions that must be tested. For every non-root
    # node v, there is either a change or a retention at v which gives rise to 2**(num_nodes - 1) many combinations.
    num_nodes = len(nodes)
    num_combinations = 2**(num_nodes - 1)
    # find the digits of num_combinations in binary representation. A combination can be represented as a binary list of
    # length digits_num_comb
    digits_num_comb = num_nodes - 1

    combinations = [None] * num_combinations
    for i in range(num_combinations):
        # convert i to binary string
        binary_i = "{0:b}".format(i)
        # convert that to binary list
        combination = list(map(int, binary_i))
        # attach missing zeros at head
        combination = [0] * (digits_num_comb - len(combination)) + combination
        combinations[i] = combination

    # get non-root nodes and sort them. Given a certain combination, every non-root node will be assigned either an
    # innovation or a retention
    non_roots = list({node for node in nodes if node != root})
    non_roots.sort()

    # get change probabilities along each edge
    change_probs = dict()
    for edge in edges:
        t = edges[edge]
        change_probs[edge] = 1 - math.exp(-c*t)

    # for every non_root v get the probability that there is a innovation from the parent of v to v
    non_root_change_probs = dict()
    for node in non_roots:
        non_root_change_probs[node] = change_probs[(parents[node], node)]

    # calculate likelihoods meaning-wise
    meaning_lhs = dict()
    for m in meanings:
        meaning_lhs[m] = lh_bf_one_meaning_step(tree,cog_data[m],non_roots,non_root_change_probs,combinations)

    # get total log likelihood
    total_log_lh = sum(map(math_tools.inf_log10, meaning_lhs.values()))
    return [total_log_lh,meaning_lhs]


# help function that calculates the likelihood for one meaning given the brute force method
def lh_bf_one_meaning_step(tree,one_meaning_data,non_roots,non_root_change_probs,combinations):
    root = tree[2]
    parents = tree[5]
    children = tree[6]

    # generate equality matrix for data nodes based on the cognate data for the meaning
    data_nodes = sorted(one_meaning_data.keys())
    num_data_nodes = len(data_nodes)
    data_equality_matrix = np.zeros((num_data_nodes, num_data_nodes))
    for j in range(num_data_nodes):
        for k in range(num_data_nodes):
            if one_meaning_data[data_nodes[j]] == one_meaning_data[data_nodes[k]]:
                data_equality_matrix[j, k] = 1

    # initialise likelihood of meaning
    lh = 0

    for combination in combinations:
        # translate combination into a dictionary
        combination_dict = {non_roots[i]:combination[i] for i in range(len(non_roots))}

        # calculate likelihood of combination (ignoring given cognate data)
        lh_combination = 1
        for node in combination_dict:
            if combination_dict[node] == 1:
                lh_combination *= non_root_change_probs[node]
            else:
                lh_combination *= (1 - non_root_change_probs[node])

        # go through tree topology and generate cognates for given combination
        combination_data = dict()
        combination_data[root] = 0
        max_int_used = 0

        if root in children:
            current_level = children[root]
        else:
            current_level = set()

        while current_level:
            for node in current_level:
                parent = parents[node]
                if combination_dict[node] == 1:
                    combination_data[node] = max_int_used + 1
                    max_int_used += 1
                else:
                    combination_data[node] = combination_data[parent]
            current_level = tree_tools.get_node_set_children(tree, current_level)

        # generate equality matrix based on the generated data
        generated_equality_matrix = np.zeros((num_data_nodes, num_data_nodes))
        for j in range(num_data_nodes):
            for k in range(num_data_nodes):
                if combination_data[data_nodes[j]] == combination_data[data_nodes[k]]:
                    generated_equality_matrix[j, k] = 1

        # test whether the equality matrix corresponding to the combination is compatible with the provided data
        if not np.array_equal(generated_equality_matrix, data_equality_matrix):
            lh_combination = 0
        else:
            pass

        # add likelihood of given combination to overall likelihood
        lh += lh_combination
    return lh

