from trees import tree_tools, tree_paths
import math
from small_tools import math_tools as mt
from operator import mul
from functools import reduce


# computes the likelihood of the data in patterns given a tree with branch lengths under the Stochastic Dollo model.
# patterns is a list of sets, each corresponding to a trait. For every set in patterns, the set contains exactly the
# nodes in tree that have the trait.
# Corresponds to equation (3) in Nicholls and Gray 2008
# If as_log_lh is true, gives the log likelihood, otherwise gives the likelihood
def compute_lh(tree, b, d, patterns, as_log_lh):
    edges = tree[1]
    edge_survival_probs = get_edge_survival_probs(edges, d)
    death_probs = get_death_probs(tree, edge_survival_probs)
    nodes_to_patterns_probs = dict()
    for pattern in set(patterns):
        nodes_to_this_pattern_prob = get_all_node_to_pattern_probs(tree, edge_survival_probs, death_probs, pattern)
        nodes_to_patterns_probs[pattern] = nodes_to_this_pattern_prob
    [registered_birth_intensity, pattern_intensities] = \
        get_intensities(tree, b, d, edge_survival_probs, death_probs, nodes_to_patterns_probs, patterns)
    if not as_log_lh:
        lh = get_lh_from_intensities(registered_birth_intensity, pattern_intensities)
    else:
        lh = get_log_lh_from_intensities(registered_birth_intensity, pattern_intensities)
    return lh


def get_lh_from_intensities(registered_birth_intensity, pattern_intensities):
    n = len(pattern_intensities)
    lh = 1 / math.factorial(n) * math.exp(-registered_birth_intensity) * reduce(mul, pattern_intensities, 1)
    return lh


def get_log_lh_from_intensities(registered_birth_intensity, pattern_intensities):
    n = len(pattern_intensities)
    log_pattern_intensities = map(mt.inf_log, pattern_intensities)
    log_lh = mt.inf_log(1) - mt.inf_log(math.factorial(n)) - registered_birth_intensity  + sum(log_pattern_intensities)
    return log_lh


# calculates the registered birth intensity and all pattern intensities
def get_intensities(tree, b, d, edge_survival_probs, death_probs, nodes_to_patterns_probs, patterns):
    registered_birth_intensity = get_registered_birth_intensity(tree, b, d, edge_survival_probs, death_probs)
    pattern_intensities = list()
    for pattern in patterns:
        nodes_to_this_pattern_probs = nodes_to_patterns_probs[pattern]
        this_pattern_intensity = get_pattern_intensity(tree, b, d, edge_survival_probs, nodes_to_this_pattern_probs,
                                                       pattern)
        pattern_intensities.append(this_pattern_intensity)
    return[registered_birth_intensity, pattern_intensities]


# for a dictionary of edges, calculates the probability that a trait survives along the edge
def get_edge_survival_probs(edges,d):
    edge_survival_probs = dict()
    for edge in edges:
        t = edges[edge]
        edge_survival_probs[edge] = math.exp(-d * t)
    return edge_survival_probs


# for any node, calculates the probability that a trait present in the node does not survive down to any leaf
# Corresponds to equation (5) in Nicholls and Gray 2008
def get_death_probs(tree,edge_survival_probs):
    children = tree[6]

    u = dict()
    levels = tree_tools.get_levels(tree)
    if levels:
        for node in levels[0]:
            u[node] = 0
    for level in levels[1:]:
        for node in level:
            children_death_probs = {child:u[child] for child in children[node]}
            u[node] = death_probs_calculation_step(node, edge_survival_probs, children_death_probs)
    return u


# calculates the probability that a trait present in node does not survive down to any leaf, given the corresponding
# probabilities in the nodes children. node must not be a leaf
def death_probs_calculation_step(node, edge_survival_probs, children_death_probs):
    children = children_death_probs.keys()
    dies_in_child_branch = dict()
    for child in children:
        survives_to_child = edge_survival_probs[(node, child)]
        dies_in_child_branch[child] = 1 - survives_to_child + survives_to_child * children_death_probs[child]
    node_death_prob = reduce(mul, dies_in_child_branch.values(), 1)
    return node_death_prob


# calculates the "intensity" that a trait was born in the tree and was registered in the data, that is it has not died
# out in all branches below its birth point. Corresponds to equation (4) in Nicholls and Gray 2008
def get_registered_birth_intensity(tree,b,d,edge_survival_probs,death_probs):
    root = tree[2]
    tree_with_adam = tree_tools.add_adam_node(tree)
    adam_node = tree_with_adam[2]
    edge_survival_probs[(adam_node, root)] = 0
    edges = tree_with_adam[1]
    edge_contributions = dict()
    for edge in edges:
        child = edge[1]
        edge_contributions[edge] = (1 - death_probs[child])*(1 - edge_survival_probs[edge])
    registered_birth_intensity = b / d * sum(edge_contributions.values())
    return registered_birth_intensity


# recursively calculates the probability that of the leaves present in the subtree below node, a trait present in node
# has survived exactly to the leaves present in pattern. The parameter pattern does not has to be a subset of all the
# leaves in the subtree below node, but nodes in pattern that are not present in the subtree below node are disregarded.
# Corresponds to equation (7) in Nicholls and Gray 2008
def get_node_to_pattern_prob(node,tree,edge_survival_probs,death_probs,pattern):
    leaves = tree[3]
    children = tree[6]

    if node in leaves:
        if node in pattern:
            pattern_prob = 1
        else:
            pattern_prob = 0
    else:
        child_branch_contributions = dict()
        for child in children[node]:
            survives_to_child = edge_survival_probs[(node,child)]
            restricted_pattern = pattern.intersection(tree_tools.get_subtree_nodes(tree, child))
            if restricted_pattern == set():
                child_branch_contributions[child] = 1 - survives_to_child + survives_to_child*death_probs[child]
            else:
                child_branch_contributions[child] = survives_to_child*\
                    get_node_to_pattern_prob(child,tree,edge_survival_probs,death_probs,restricted_pattern)
        pattern_prob = reduce(mul, child_branch_contributions.values(), 1)
    return pattern_prob


# calculates all node_to_pattern_probs, from the leaves upwards
def get_all_node_to_pattern_probs(tree,edge_survival_probs,death_probs,pattern):
    nodes = tree[0]
    children = tree[6]
    pattern_probs = dict()
    subtree_nodes = dict()
    for node in nodes:
        subtree_nodes[node] = tree_tools.get_subtree_nodes(tree,node)
    levels = tree_tools.get_levels(tree)
    if levels:
        for node in levels[0]:
            if node in pattern:
                pattern_probs[node] = 1
            else:
                pattern_probs[node] = 0
    for level in levels[1:]:
        for node in level:
            children_pattern_probs = {child:pattern_probs[child] for child in children[node]}
            pattern_probs[node] = node_to_pattern_prob_calculation_step(
                node, subtree_nodes, edge_survival_probs, death_probs, children_pattern_probs, pattern)
    return pattern_probs


# calculates the node_to_pattern_prob of node given the corresponding probabilities of the children of node. node must
# not be a leaf
def node_to_pattern_prob_calculation_step(node, children_subtree_nodes, edge_survival_probs, death_probs,
                                          children_to_pattern_probs, pattern):
    children = children_to_pattern_probs.keys()
    child_branch_contributions = dict()
    for child in children:
        survives_to_child = edge_survival_probs[(node, child)]
        subtree_nodes = children_subtree_nodes[child]
        restricted_pattern = pattern.intersection(subtree_nodes)
        if restricted_pattern == set():
            child_branch_contributions[child] = 1 - survives_to_child + survives_to_child * death_probs[child]
        else:
            child_branch_contributions[child] = survives_to_child * children_to_pattern_probs[child]
    pattern_prob = reduce(mul, child_branch_contributions.values(), 1)
    return pattern_prob


# calculates the "intensity" that a trait was born in the tree and has survived exactly to the nodes
# present in pattern (note this is not a probability since it scales with the birth rate)
# Corresponds to equation (6) in Nicholls and Gray 2008
def get_pattern_intensity(tree, b, d, edge_survival_probs, nodes_to_pattern_probs, pattern):
    root = tree[2]
    tree_with_adam = tree_tools.add_adam_node(tree)
    adam_node = tree_with_adam[2]
    mrca = tree_tools.get_set_mrca(tree_with_adam, pattern)
    root_path = tree_paths.get_node_to_root_path(tree_with_adam, mrca)
    edge_survival_probs[(adam_node, root)] = 0

    edge_contributions = dict()
    for edge in root_path:
        child = edge[1]
        child_gives_pattern = nodes_to_pattern_probs[child]
        edge_contributions[edge] = child_gives_pattern*(1 - edge_survival_probs[edge])
    pattern_intensity = b / d * sum(edge_contributions.values())
    return pattern_intensity
