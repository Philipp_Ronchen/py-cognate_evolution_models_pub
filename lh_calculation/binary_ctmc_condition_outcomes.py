from lh_calculation import lh_binary_ctmc as lhbc
from small_tools import math_tools


def adjust_lh_for_0_data(lh, tree, b, d, f, data_nodes, num_parallel_values):
    no_vanished_trait_prob = get_no_vanished_trait_prob(tree, b, d, f, data_nodes, num_parallel_values)
    adjusted_lh = lh / no_vanished_trait_prob
    return adjusted_lh


def adjust_log_lh_for_0_data(log_lh, tree, b, d, f, data_nodes, num_parallel_values):
    no_vanished_trait_prob = get_no_vanished_trait_prob(tree, b, d, f, data_nodes, num_parallel_values)
    adjusted_log_lh = log_lh - math_tools.inf_log(no_vanished_trait_prob)
    return adjusted_log_lh


def get_no_vanished_trait_prob(tree, b, d, f, data_nodes, num_parallel_values):
    data = {node:0 for node in data_nodes}
    # print("data")
    # print(data)
    # print("num parallel values")
    # print(num_parallel_values)
    one_class_zero_prob = lhbc.calculate_lh(tree, b, d, f, data)
    #print(one_class_zero_prob)
    one_class_nonzero_prob = 1 - one_class_zero_prob
    #print(one_class_nonzero_prob)
    all_classes_no_zero_prob = one_class_nonzero_prob ** num_parallel_values
    #print(all_classes_no_zero_prob)
    return all_classes_no_zero_prob
