from lh_calculation import lh_general_finite_state_markov as lhg
from lh_calculation import binary_ctmc_condition_outcomes as cond_outcomes
from small_tools import math_tools
import math
# functions to calculate likelihoods in the 2-state CTMC model. The states are denoted 0 and 1 and the model is
# parametrised by a birth rate b and a death rate d.
# based on the algorithm for the general finite state markov model


def calculate_parallel_data_log_lh(tree, b, d, root_freqs, parallel_data, adjust_for_0_data = False):
    nodes = tree[0]
    edges = tree[1]
    states = dict()
    data_nodes = parallel_data.keys()
    for node in nodes:
        states[node] = {0, 1}
    num_parallel_values = get_num_parallel_values(parallel_data)
    #root_freqs = estimate_root_freqs_from_data(num_parallel_values, parallel_data)
    edge_transition_probs = get_edge_transition_probs(edges, b, d)
    log_lh = lhg.calculate_parallel_data_log_lh(tree, states, edge_transition_probs, root_freqs, num_parallel_values,
                                            parallel_data)
    if adjust_for_0_data == True:
        no_vanished_traits_prob = cond_outcomes.get_no_vanished_trait_prob(tree, b, d, root_freqs, data_nodes, num_parallel_values)
        log_lh -= math_tools.inf_log(no_vanished_traits_prob)
    return log_lh


def calculate_lh(tree, b, d, root_freqs, data, adjust_for_0_data=False):
    nodes = tree[0]
    edges = tree[1]
    states = dict()
    data_nodes = data.keys()
    for node in nodes:
        states[node] = {0, 1}
    edge_transition_probs = get_edge_transition_probs(edges, b, d)
    lh = lhg.calculate_lh(tree, states, edge_transition_probs, root_freqs, data)
    if adjust_for_0_data == True:
        no_vanished_traits_prob = cond_outcomes.get_no_vanished_trait_prob(tree, b, d, root_freqs, data_nodes, 1)
        lh /= math_tools.inf_log(no_vanished_traits_prob)
    return lh


def lh_from_root_clhs(root_clhs, root_freqs):
    return lhg.lh_from_root_clhs(root_clhs, root_freqs)


def calculation_step(child_edges, child_clhs, b, d):
    node_children = child_clhs.keys()
    node_states = {0, 1}
    child_states = {child:{0, 1} for child in node_children}
    child_edge_transition_probs = get_edge_transition_probs(child_edges,b,d)
    node_clhs = lhg.calculation_step(node_states, child_states, child_clhs, child_edge_transition_probs)
    return node_clhs


def get_leaf_clhs(node, data):
    node_states = {0,1}
    node_clhs = lhg.get_leaf_clhs(node, node_states, data)
    return node_clhs


# makes use of the closed form solution of the transition probabilities of ctmc's which exists for two-state ctmc's
def get_edge_transition_probs(edges,b,d):
    edge_transition_probs = dict()
    for edge in edges:
        t = edges[edge]
        current_transition_probs = dict()
        current_transition_probs[(0, 0)] = d / (b + d) + b / (b + d) * math.exp(-(b + d) * t)
        current_transition_probs[(0, 1)] = b / (b + d) - b / (b + d) * math.exp(-(b + d) * t)
        current_transition_probs[(1, 0)] = d / (b + d) - d / (b + d) * math.exp(-(b + d) * t)
        current_transition_probs[(1, 1)] = b / (b + d) + d / (b + d) * math.exp(-(b + d) * t)
        edge_transition_probs[edge] = current_transition_probs
    return edge_transition_probs


# estimates the root frequencies by counting which fraction of cognate classes a data node inhabits on average
def estimate_root_freqs_from_data(num_parallel_values, data):
    num_data_nodes = len(data.keys())
    num_data_entries = num_data_nodes*num_parallel_values
    sum_traits = sum(map(sum,data.values()))
    freq_0 = (num_data_entries - sum_traits) / num_data_entries
    freq_1 = sum_traits / num_data_entries
    root_freqs = (freq_0, freq_1)
    return root_freqs


# gets the number of parallel values in the data (the number of binary cognate classes)
def get_num_parallel_values(data):
    if not data:
        raise ValueError("data is empty")
    else:
        return len(list(data.values())[0])


