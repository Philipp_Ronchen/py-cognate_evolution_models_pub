import math
import more_itertools
from operator import mul
from functools import reduce
from small_tools import dict_tools
from small_tools import gmp2_math_tools as math_tools
from gmpy2 import exp, fsum, mul
from trees import tree_tools


# calculates the likelihood of a tree given cog_data recursively, with the algorithm given in the paper
def compute_lh(tree, change_rates, cog_data):
    edges = tree[1]

    # get meanings in the data
    meanings = set(cog_data.keys())

    # calculate likelihoods meaning-wise
    meaning_lhs = dict()
    for m in meanings:
        # get change and retention probabilities along all edges
        [change_probs, ret_probs] = get_change_ret_probs(edges, change_rates[m])
        meaning_lhs[m] = lh_one_meaning(tree, change_probs, ret_probs, cog_data[m])

    # get total log likelihood
    total_log_lh = sum(map(math_tools.inf_log, meaning_lhs.values()))
    return [total_log_lh, meaning_lhs]


# help function that calculates the likelihood for one meaning given the recursive method
def lh_one_meaning(tree, change_probs, ret_probs, one_meaning_data):
    root = tree[2]
    children = tree[6]

    extended_data = extend_data(tree, one_meaning_data)
    if extended_data == "contradiction":
        lh = 0
    else:
        p = recursion_step(root,children,change_probs,ret_probs,extended_data[1])[0]
        lh = p
    return lh


# calculates the extension of one-meaning data, if it exists. Returns [True, extended] if the extension exists and
# [False, contradicatory_data] if it does not, where contradictory_data is a dictionary that assigns to every node that
# has been assigned multiple distinct values the values it has been assigned to.
def extend_data(tree, one_meaning_data):
    nodes = tree[0]
    root = tree[2]
    leaves = tree[3]
    parents = tree[5]
    children = tree[6]

    # stores for every node all the data values that occur in the subtree below the node, and assigns every such value
    # its "multiplicity". The multiplicity of the value at the node is the number of children of the node which have
    # the node in their subtree, plus one if the node is determined to have the value directly from the data
    subtree_values = {node:dict() for node in nodes}

    # stores for every node all the subtree_values of its parent which have multiplicity at least 2. These are exactly
    # the values which cannot have been propagated upwards from the node to its parent alone, so they must occur
    # somewhere else in the tree (not only in the subtree of node)
    ancestral_values = {node:set() for node in nodes}

    # stores all nodes which are determined by the extension algorithm and the values the node is determined to have. If
    # a node is determined to be multiple values, the data is contradictory
    determined_values = dict()

    # first iteration, from the leaves upwards, used to determined subtree_values
    current_level = leaves
    calculated = set()
    while current_level:
        for node in current_level:
            if node in one_meaning_data:
                dict_tools.dict_add_or_increase(subtree_values[node], one_meaning_data[node])
            if node in children:
                for child in children[node]:
                    for cog_value in subtree_values[child]:
                        dict_tools.dict_add_or_increase(subtree_values[node], cog_value)
        calculated = calculated.union(current_level)
        current_level = tree_tools.get_new_node_set_parents(tree, calculated)

    # second iteration, from the root downwards, used to determined ancestral_values
    ancestral_values[root] = set()
    if root in children:
        current_level = children[root]
    else:
        current_level = set()
    while current_level:
        for node in current_level:
            parent = parents[node]
            ancestral_values[node] = ancestral_values[parent].union(
                {cog_value for cog_value in subtree_values[parent] if subtree_values[parent][cog_value] >= 2})
        current_level = tree_tools.get_node_set_children(tree, current_level)

    # nodes are determined if and only if one of the following points hold:
    # - they are in the (non-extended) data
    # - they have a subtree_value with multiplicity at least 2
    # - they have a subtree_value which is also an ancestral_value
    for node in nodes:
        if node in one_meaning_data:
            dict_tools.dict_add_to_set(determined_values, node, one_meaning_data[node])
        for cog_value in subtree_values[node]:
            if subtree_values[node][cog_value] >= 2 or cog_value in ancestral_values[node]:
                dict_tools.dict_add_to_set(determined_values, node, cog_value)

    contradictory_nodes = dict()
    for node in nodes:
        if node in determined_values and len(determined_values[node]) >= 2:
            contradictory_nodes[node] = determined_values[node]
    if contradictory_nodes:
        return [False, determined_values]
    else:
        extended_data = {node: list(determined_values[node])[0] for node in nodes if node in determined_values}
        return [True, extended_data]


# recursive step calculating the subtree likelihood p of node_to_calculate as well as the quantity q. Works only if the
# extended_data is non-contradictory
def recursion_step(node_to_calculate,children,change_probs,ret_probs,extended_data):
    # calculate p and q for children
    if node_to_calculate in children:
        node_children = children[node_to_calculate]
    else:
        node_children = set()
    children_results = {child:recursion_step(child,children,change_probs,ret_probs,extended_data)
                        for child in node_children}
    [new_p, new_q] = calculation_step(node_to_calculate,children_results,change_probs,ret_probs,extended_data)
    return [new_p, new_q]


# calculates the subtree likelihood p of node_to_calculate as well as the quantity q, given the results for the
# children of node_to_calculate. Works only if the extended_data is non-contradictory
def calculation_step(node_to_calculate,children_results,change_probs,ret_probs,extended_data):
    new_p = 0
    new_q = 0
    N = set(children_results.keys())

    p = {child: children_results[child][0] for child in N}
    q = {child: children_results[child][1] for child in N}

    # calculate subtree likelihoods like in the paper
    if node_to_calculate not in extended_data:
        subsets = more_itertools.powerset(N)
        for I in subsets:
            I = set(I)
            J = N.difference(I)
            prod_J = reduce(mul, [(p[child] - q[child]) for child in J], 1)
            prod_I = reduce(mul, [change_probs[(node_to_calculate, child)] * q[child] for child in I], 1)
            prod_I0 = dict()
            for child_0 in I:
                prod_I0[child_0] = reduce(mul, [change_probs[(node_to_calculate, child)] * q[child]
                                                for child in I.difference({child_0})], 1) \
                                   * ret_probs[(node_to_calculate, child_0)] * q[child_0]
            sigma_1_I = prod_J * prod_I
            sigma_2_I = sum([prod_J * prod_I0[child_0] for child_0 in I])
            new_p += sigma_1_I + sigma_2_I
            new_q += sigma_2_I
    else:
        L = {child for child in N if
             child in extended_data and extended_data[child] == extended_data[node_to_calculate]}
        subsets = more_itertools.powerset(N.difference(L))
        for I in subsets:
            J = N.difference(L).difference(I)
            prod_J = reduce(mul, [(p[child] - q[child]) for child in J], 1)
            prod_L = reduce(mul, [ret_probs[(node_to_calculate, child)] * p[child] for child in L], 1)
            prod_I = reduce(mul, [change_probs[(node_to_calculate, child)] * q[child] for child in I], 1)
            sigma_3_L_I = prod_J * prod_L * prod_I
            new_p += sigma_3_L_I
            new_q += sigma_3_L_I
    return [new_p, new_q]


# calculates the change and retention probs along the edges in edges with the change rate c
def get_change_ret_probs(edges, c):
    change_probs = dict()
    ret_probs = dict()
    for edge in edges:
        t = edges[edge]
        change_probs[edge] = 1 - math.exp(-c * t)
        ret_probs[edge] = math.exp(-c * t)
    return [change_probs, ret_probs]