# General information

This repository gives code associated with the article *Likelihood calculation in a multistate model of vocabulary evolution for linguistic dating.* It contains:

1. A simple tree data format
2. An implementation of the recursive likelihood algorithm from the paper
3. An implementation of a simple "brute force" likelihood algorithm, which has exponential runtime but can be compared with the recursive algorithm for small trees
4. An implementation of the likelihood algorithm for the Stocastic Dollo model from the paper "Nicholls and Gray 2008: Dated ancestral trees from binary trait data and their application to the diversification of languages". Plus an inplmentation of a minor modification of the Stochastic Dollo model, called meaningwise Stochastic Dollo model (SDM).
5. An implementation of the likelihood algorithm (simple Felsenstein pruning, see Felstenstein 1973) of the generalised continous time Markov chain model with two state, called Binary CTMC model.
6. Implementations of the generative Multistate, Stochastic Dollo and CTMC processes.
7. Methods to effectively run the Multistate, the Stochastic Dollo and the Binary CTMC algorithms for a given tree topology with different branch lengths.
8. Some test trees and generated test data (no "real" data), as well as some IO functions

# Data format

### Change rates

The multistate model is parametrised by a change rate `c`, meanings can have different change rates, this is usually stored as a dictionary, and can look like the following:
```python
{ "m0":0.3, "m1":0.7 }
```


### Trees

A `tree` is a list of the form `[nodes,edges,root,leaves,internals,parents,children]`,  where: 
 - `nodes` is a set (of strings for example).
 - `edges` is a dictionary indexed by tuples (a,b) where a and b are in nodes. a is the parent and b is the child node.  The value of (a,b) is the edge length.
 - `root` is an element of nodes and specifies the tree root.
 - `leaves` is the subset of nodes that are leaves in the tree. It can be computed by `get_leaves` from `tree_basics.py`.
 - `internals` is the subset of nodes that are not leaves in the tree. It can be computed by `get_internals`.
 - `parents` is a dictionary, associating to every non-root node its parent. It can be computed by `get_parents`.
 - `children` is a dictionary, associating to every non-leaf node a set of its children. It can be computed by `get_children`.

We have:
```python
tree[0] = nodes 
tree[1] = edges
tree[2] = root
tree[3] = leaves
tree[4] = internals
tree[5] = parents
tree[6] = children
```

`nodes` and `edges` together always contain the complete information about the tree. If the tree is not a single leaf, `edges`  contains the complete information about the tree. `edges `is the only data variable that stores information about branch lengths and not only tree topology. Sometimes, but not consistently, a tree whose edge lengths are irrelevant or unspecified is called topology.

### Cognate data

Multistate cognate data is stored as a dictionary of dictionaries. The dictionary `cog_data` associates to every meaning m a dictionary that associates to some nodes (or possibly all nodes) a natural number. This number represents the cognate class for this node. Cognate data can look like the following:

```python
{ "m0":{"a":0, "b":0, "c":1, "d":2, "e":3}, "m1":{"a":0, "b":1, "c":2, "d":1, "e":3} }
```

Here, for the meanings "m0" and "m1", we are given data for the nodes "a", "b", "c", "d" and "e". Sometimes we are 
forced to work with "setwise multistate" data. Here for every meaning a node is associated a set of possible values.
The Multistate likelihood algorithm cannot be applied to such setwise work on such setwise
data. However, such data can be "extended" in a straight-forward way, as a way to compute the contradictions in the 
data.

Binary cognate data is stored in one of three different ways. 
1. As a dictionary of binary lists (of zeros and ones) denoting the presence and absence of traits.
2. With the *traits* being primary, a traits are here coded as natural numbers and to nodes is associated a set 
containing the traits the node exhibits. This is the same data format as used for "setwise" multistate data,
apart from that no meaning structure is given.
3. As a list of so-called *patterns*: A pattern is a frozenset of leaves that have a certain trait. 
Assuming that all "a","b","c","d","e" are leaves, the above multistate data would be translated to the following binary data:

Presence-absence format
```python
{"a":[1,0,0,0,1,0,0,0], "b":[1,0,0,0,0,1,0,0], "c":[0,1,0,0,0,0,1,0], "d":[0,0,1,0,0,1,0,0], "e":[0,0,0,1,0,0,0,1]}
```

Patterns format
```python
[frozenset({"a","b"}),frozenset({"c"}),frozenset({"d"}),frozenset({"e"}),frozenset({"a"}),frozenset({"b,d"}),frozenset({"c"}),frozenset({"e"})]
```

### accum_trees and value_trees

To efficiently calculate likelihoods for many different branch lenghts, it is sometimes necessary to construct trees while doing likelihood calculations at the same time. This is carried out in the module `build_trees_and_accum.py` in `branch_length_tree_operations`. The functions in this module use an intermediate data format called `accum_trees` of the form
```python
{node:{age:[[tree_edges,accumulated_value]..]...}...}
```
It assigns to a node a dictionary with ages as keys, and to every age a list of trees with the node as a root carrying an accumulated value. accum_trees are translated to value_trees:

A `value_tree` is a list of the form
```python
[tree_edges, node_ages, value]
```
where node_ages is dictionary assigning to every node an age, it can be computed from tree_edges and the other way.
On value trees various calculations are carried out, for example in some cases we want to calculate a tree likelihood 
according to a certain substitution model from the value given in a value_tree.