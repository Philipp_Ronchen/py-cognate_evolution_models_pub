from trees import binary_test_trees as btt
import copy
from generative_models import generate_multistate as gm
from data_transform import multistate_data_transf as mdt
from data_transform import generate_data_combinations as gdc
from branch_length_tree_operations import accum_functions as af
from branch_length_tree_operations import build_trees_and_accum as bta
from branch_length_tree_operations import accum_results_transf as art
from branch_length_tree_operations import max_lh as max_lh
from lh_calculation import lh_stochastic_dollo as lhsd
from small_tools import dict_tools
import math
import numpy
import time
import random as rd


start_time = time.time()

topology = btt.b4l_tree_1
age_constraints = {"a":[0,0], "b":[0,0], "c":[0,0],"d":[0,0],"e":[400,800],"f":[800,1200],"g":[1200,1600]}
max_tree_height = 1800
age_resolution = 200
root = topology[2]
leaves = topology[3]

tree = copy.deepcopy(topology)
tree[1] = {("e","a"):600,("e","b"):600,("f","e"):400,("f","c"):1000, ("g","d"):1400, ("g","f"):400}

num_meanings = 6
meanings = {"m" + str(i) for i in range(num_meanings)}
base_rate = 0.001

all_one_meaning_outcomes = gdc.get_all_multistate_data_sets(leaves)
meanings = {"m" + str(i) for i in range(num_meanings)}
possible_meaning_outcomes = {m:all_one_meaning_outcomes for m in meanings}
all_outcomes = list(dict_tools.dict_product(possible_meaning_outcomes))

cog_data = rd.sample(all_outcomes, 1)[0]

print("cog data")
print(cog_data)



sd_data = mdt.multistate_to_patterns(cog_data)
binary_data = mdt.multistate_to_binary(cog_data, leaves)

num_cog_classes = len(binary_data["a"])
print("num cog classes")
print(num_cog_classes)

################# TRUE SD LH ##############################
b = base_rate
d = base_rate

true_lh = lhsd.compute_lh(tree, b, d, sd_data, True)
print("SD lh of true tree with true parameters")
print(true_lh)

#############################################################





sd_dlist = numpy.linspace(base_rate / 10, base_rate * 2, 10)
sd_blist = numpy.linspace(num_meanings * base_rate / 10, num_meanings * base_rate * 2, 10)
ctmc_dlist = numpy.linspace(base_rate / 10, base_rate * 2, 10)
ctmc_blist = copy.deepcopy(ctmc_dlist)

flist = [(f, 1 - f) for f in numpy.linspace(0, 1, 11)]

# ##################### SD ######################################################################

sd_max_lh_tree = max_lh.sd_max_lh_free_parameters(topology, sd_blist, sd_dlist, max_tree_height, age_constraints, age_resolution, sd_data)

print("sd max likelihood parameters and tree")
sd_parameters = sd_max_lh_tree[0]
sd_best_tree = sd_max_lh_tree[1]
sd_lh = sd_best_tree[2]
print(sd_max_lh_tree)
print(sd_best_tree)
print(sd_lh)

# ##################### BINARY CTMC ######################################################################



binary_ctmc_max_lh_tree = max_lh.binary_ctmc_max_lh_free_parameters(topology, ctmc_blist, ctmc_dlist, flist, max_tree_height,
                                                                    age_constraints, age_resolution, binary_data)
print("binary ctmc max likelihood parameters and trees")
ctmc_parameters = binary_ctmc_max_lh_tree[0]
ctmc_best_tree = binary_ctmc_max_lh_tree[1]
ctmc_lh = ctmc_best_tree[2]
ctmc_adjusted_lh = math.log(math.factorial(num_cog_classes)) + ctmc_lh
print(binary_ctmc_max_lh_tree)
print(ctmc_best_tree)
print(ctmc_lh)
print(ctmc_adjusted_lh)

end_time = time.time()
print("elapsed time:")
print(end_time - start_time)
