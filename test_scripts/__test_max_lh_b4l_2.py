from generative_models import generate_multistate as gm
from branch_length_tree_operations import max_lh
from branch_length_tree_operations import build_trees_and_accum as bta
from branch_length_tree_operations import accum_results_transf as art
from branch_length_tree_operations import accum_functions as af
from data_transform import multistate_data_transf as mdt
from data_transform import multistate_data_to_sd as mdsd
from trees import binary_test_trees as btt
import numpy
import copy

topology = btt.b4l_tree_2
age_constraints = {"a":[0,0], "b":[0,0], "c":[0,0],"d":[0,0],"e":[400,1000],"f":[400,1000],"g":[1200,1800]}
max_tree_height = 1800
age_resolution = 100
root = topology[2]
leaves = topology[3]

tree = copy.deepcopy(topology)
tree[1] = {("e","a"):700,("e","b"):700,("f","c"):700,("f","d"):700, ("g","e"):800, ("g","f"):800}

num_meanings = 200
meanings = {"m" + str(i) for i in range(num_meanings)}
change_rate = 0.001
change_rates = {m:change_rate for m in meanings}
cog_data = gm.generate_outcome(tree, change_rate, num_meanings)
cog_data = mdt.restrict_data_to_nodes(cog_data, leaves)
print("generated cognate data")
print(cog_data)

data = mdt.package_data(change_rates, cog_data, tree)
sd_data = mdsd.multistate_to_patterns(cog_data)
#print(sd_data)

multistate_accum_trees = bta.build_trees_and_accumulate(tree, max_tree_height, age_constraints, age_resolution, data,
                                                    af.multistate_leaf_value_function, af.multistate_accum_func)
multistate_value_trees = art.multistate_accum_to_log_lh_trees(multistate_accum_trees, root)


multistate_max_lh_tree = max_lh.get_max_value_tree(multistate_value_trees)

print("multistate max lh tree")
print(multistate_max_lh_tree)

dlist = numpy.linspace(change_rate/10, change_rate*2, 20)
dlist = numpy.linspace(change_rate,change_rate,1)
blist = num_meanings*dlist

sd_max_lh_tree = max_lh.sd_max_lh_free_parameters(topology, blist, dlist, max_tree_height, age_constraints, age_resolution, sd_data)

print("sd max likelihood parameters and tree")
print(sd_max_lh_tree)