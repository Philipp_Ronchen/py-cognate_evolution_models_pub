from trees import binary_test_trees as btt
from small_tools import io_tools as io
from branch_length_tree_operations import meaning_lhs as ml
from data_transform import multistate_data_transf as mdt

tree_topology = btt.b4l_tree_1

max_tree_height = 200
age_constraints = {"a": [0, 0], "b": [0, 0], "c": [0, 0], "d": [0, 0]}
age_resolution = 100

cog_data = {"m0":{"a": 1, "b": 1, "c":1, "d": 1}, "m1":{"a": 1, "b": 2, "c":3, "d": 4}}
change_rates = {"m0":0.001, "m1": 0.001}
data = mdt.package_data(change_rates, cog_data, tree_topology)

meaning_lhs = ml.multistate_meaningwise(tree_topology, max_tree_height, age_constraints, age_resolution, data)
print(meaning_lhs)

multiple_cog_data = {"m0":[{"a": 1, "b": 1, "c":1, "d": 1}],
                     "m1":[{"a": 1, "b": 2, "c":3, "d": 4}, {"a": 1, "b": 2, "c":2, "d": 1}]}
multiple_data = mdt.package_multiple_data(change_rates, multiple_cog_data, tree_topology)
multiple_meaning_lhs = ml.multiple_data_multistate_meaningwise(tree_topology, max_tree_height, age_constraints,
                                                              age_resolution, multiple_data)
print(multiple_meaning_lhs)

io.draw_tree(tree_topology)