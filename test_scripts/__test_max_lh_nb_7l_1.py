from generative_models import generate_multistate as gm
from branch_length_tree_operations import max_lh
from branch_length_tree_operations import build_trees_and_accum as bta
from branch_length_tree_operations import accum_results_transf as art
from branch_length_tree_operations import accum_functions as af
from data_transform import multistate_data_transf as mdt
from data_transform import multistate_data_to_sd as mdsd
from trees import non_binary_test_trees as nbtt
from small_tools import io_tools as io
import numpy
import copy

topology = nbtt.nb7l_tree_1
root = topology[2]
leaves = topology[3]

age_constraints = {"h":[300,700], "i":[300,700], "k":[800,1400], "l":[800,1400], "m":[1100, 1900]}
for leaf in leaves:
    age_constraints[leaf] = [0,0]
max_tree_height = 1700
age_resolution = 200

tree = copy.deepcopy(topology)
tree[1] = {("h","a"):500,("h","b"):500,("h","c"):500,("m","d"):1500,("k","e"):1000,("i","f"):500,("i","g"):500,
           ("l","h"):500,("k","i"):500,("l","j"):1000,("m","k"):500,("m","l"):500}
#io.draw_tree(tree)

num_meanings = 100
meanings = {"m" + str(i) for i in range(num_meanings)}
change_rate = 0.001
change_rates = {m:change_rate for m in meanings}
cog_data = gm.generate_outcome(tree, change_rate, num_meanings)
cog_data = mdt.restrict_data_to_nodes(cog_data, leaves)
print("generated cognate data")
print(cog_data)

data = mdt.package_data(change_rates, cog_data, tree)
sd_data = mdsd.multistate_to_patterns(cog_data)
#print(sd_data)

multistate_accum_trees = bta.build_trees_and_accumulate(tree, max_tree_height, age_constraints, age_resolution, data,
                                                    af.multistate_leaf_value_function, af.multistate_accum_func)
multistate_value_trees = art.multistate_accum_to_log_lh_trees(multistate_accum_trees, root)


multistate_max_lh_tree = max_lh.get_max_value_tree(multistate_value_trees)



dlist = numpy.linspace(change_rate/10, change_rate*2, 10)
#dlist = numpy.linspace(change_rate,change_rate,1)
blist = num_meanings*dlist

sd_max_lh_tree = max_lh.sd_max_lh_free_parameters(topology, blist, dlist, max_tree_height, age_constraints, age_resolution, sd_data)

print("multistate max lh tree")
print(multistate_max_lh_tree)

print("sd max likelihood parameters and tree")
print(sd_max_lh_tree)