from trees import binary_test_trees as btt
import copy
from data_transform import multistate_data_transf as mdt
from branch_length_tree_operations import max_lh as max_lh
from data_transform import generate_data_combinations as gdc
from lh_calculation import lh_stochastic_dollo as lhsd
from generative_models import generate_multistate as gm
from small_tools import io_tools as io
import numpy
import time


def ex(num):
    start_time = time.time()
    num_meanings = 500

    topology = btt.b5l_tree_2
    age_constraints = {"a" :[0 ,0], "b" :[0 ,0], "c" :[0 ,0] ,"d" :[0 ,0] ,"e" :[0 ,0] ,"f" :[0 ,2000] ,"g" :[0 ,2000],
                       "h" :[0 ,2000], "i" :[0 ,2000]}
    max_tree_height = 2000
    age_resolution = 200
    root = topology[2]
    leaves = topology[3]

    tree = copy.deepcopy(topology)
    tree[1] = {("f","a"):600,("f","b"):600,("g","c"):1000,("g","f"):400, ("h","d"):800, ("h","e"):800, ("i","h"):600, ("i","g"):400}

    true_change_rate = 0.001
    meanings = {"m" + str(i) for i in range(num_meanings)}
    change_rates = {meaning:true_change_rate for meaning in meanings}

    multistate_outcome = gm.generate_outcome(tree, meanings, change_rates)
    multistate_outcome = mdt.restrict_data_to_nodes(multistate_outcome, leaves)
    binary_data = mdt.multistate_to_binary(multistate_outcome, leaves)

    base_rate = true_change_rate

    ctmc_dlist = numpy.linspace(base_rate / 10, base_rate * 2, 10)
    ctmc_blist = copy.deepcopy(ctmc_dlist)
    flist = [(f, 1 - f) for f in numpy.linspace(0.1, 0.9, 9)]

    binary_ctmc_max_lh_tree = max_lh.binary_ctmc_max_lh_free_parameters(topology, ctmc_blist, ctmc_dlist, flist, max_tree_height,
                                                                                age_constraints, age_resolution, binary_data)

    parameters = binary_ctmc_max_lh_tree[0]
    best_tree = binary_ctmc_max_lh_tree[1]
    ages = best_tree[1]
    lh = best_tree[2]

    print("parameters b, d, f")
    print(parameters)
    print("best_tree")
    print(best_tree)

    end_time = time.time()
    print("elapsed time")
    print(end_time - start_time)

    file = open("results_" + str(num) + ".txt", "a")
    file.write("parameters b, d, f\n")
    file.write(str(parameters) + "\n")
    file.write("best tree\n")
    file.write(str(best_tree) + "\n")
    file.write("elapsed_time\n")
    file.write(str(end_time - start_time) + "\n")
    file.close()
    return None