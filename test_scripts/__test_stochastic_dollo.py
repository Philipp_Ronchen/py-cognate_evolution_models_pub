from trees.normalised_test_trees import normalised_tree_1, infinite_edge_tree
from lh_calculation import lh_stochastic_dollo as lhsd
from generative_models import generate_sd as gsd
from small_tools import io_tools as io
from small_tools import math_tools
from trees import tree_tools

# testing get_edge_survival_probs and get_death_probs
tree = normalised_tree_1
edges = tree[1]

d = 1
edge_survival_probs = lhsd.get_edge_survival_probs(edges,d)
death_probs = lhsd.get_death_probs(tree, edge_survival_probs)
print("edge survival probs")
print(edge_survival_probs)
print("death probs")
print(death_probs)

# testing get_node_to_pattern_prob
node = "e"
pattern = frozenset({"a", "b"})
node_to_pattern_prob = lhsd.get_node_to_pattern_prob(node, tree, edge_survival_probs, death_probs, pattern)
all_node_to_pattern_probs = lhsd.get_all_node_to_pattern_probs(tree, edge_survival_probs, death_probs, pattern)
print("pattern:")
print(pattern)
print("node to pattern prob")
print(node_to_pattern_prob)
print("all node to pattern probs")
print(all_node_to_pattern_probs)

# testing get_pattern_intensity
b = 1
pattern = frozenset({"a", "c"})
pattern_intens = lhsd.get_pattern_intensity(tree, b, d, edge_survival_probs, death_probs, pattern)
print("pattern intensity")
print(pattern_intens)

# testing compute_lh
pattern = frozenset({"a","b"})
patterns = [pattern] * 100
print("pattern again")
print(pattern)
lh = lhsd.compute_lh(tree,b,d,patterns, False)
print("lh")
print(lh)
print("log of lh")
print(math_tools.inf_log(lh))
log_lh = lhsd.compute_lh(tree,b,d,patterns, True)
print("log lh")
print(log_lh)

# generate all possible_data sets
data_sets = gsd.get_possible_data_sets(tree, 4)
print(data_sets)

# testing if the sum of likelihoods approaches 1 if as many as possible data sets are considered and their likelihoods
# added up
print("lh sum")
print(gsd.calculate_possible_data_lh_sum(tree,b,d,6))


# # calculate likelihoods on a tree consisting of a single infinite branch to check if equilibrium distribution is correct
# tree = infinite_edge_tree
# data_sets = gsd.get_possible_data_sets(tree,5)
# print(data_sets)
# for patterns in data_sets:
#     print(patterns)
#     print(lhsd.compute_lh(tree,b,d,patterns,False))


io.draw_tree(tree)