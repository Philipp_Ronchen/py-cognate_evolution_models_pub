from generative_models import generate_multistate as gm
from branch_length_tree_operations import max_lh
from branch_length_tree_operations import build_trees_and_accum as bta
from branch_length_tree_operations import accum_results_transf as art
from branch_length_tree_operations import accum_functions as af
from data_transform import multistate_data_transf as mdt
from data_transform import multistate_data_to_sd as mdsd
from trees import non_binary_test_trees as nbtt
from small_tools import io_tools as io
import numpy
import copy

topology = nbtt.nb14l_tree
root = topology[2]
leaves = topology[3]


age_constraints = {"o":[400,600], "p":[400,600], "q":[900,1100], "r":[900,1100], "s":[900,1100], "u":[1400, 1600],
                   "t":[1400,1600], "v":[1900,2100]}
for leaf in leaves:
    age_constraints[leaf] = [0,0]
max_tree_height = 2300
age_resolution = 100

tree = copy.deepcopy(topology)
tree[1] = {("q","a"):1000,("q","b"):1000,("q","c"):1000,("q","d"):1000,("o","e"):500,("o","f"):500,("p","g"):500,
           ("p","h"):500,("r","i"):1000,("r","j"):1000,("u","k"):1500,("s","l"):1000,("s","m"):1000,("s","n"):1000,
           ("t","o"):1000,("u","p"):1000,("t","q"):500,("u","r"):500,("v","s"):1000,("v","t"):500,("v","u"):500}

num_meanings = 50
meanings = {"m" + str(i) for i in range(num_meanings)}
change_rate = 0.001
change_rates = {m:change_rate for m in meanings}
cog_data = gm.generate_outcome(tree, change_rate, num_meanings)
cog_data = mdt.restrict_data_to_nodes(cog_data, leaves)
print("generated cognate data")
print(cog_data)

data = mdt.package_data(change_rates, cog_data, tree)
sd_data = mdsd.multistate_to_patterns(cog_data)
#print(sd_data)

multistate_accum_trees = bta.build_trees_and_accumulate(tree, max_tree_height, age_constraints, age_resolution, data,
                                                    af.multistate_leaf_value_function, af.multistate_accum_func)
multistate_value_trees = art.multistate_accum_to_log_lh_trees(multistate_accum_trees, root)


multistate_max_lh_tree = max_lh.get_max_value_tree(multistate_value_trees)



#dlist = numpy.linspace(change_rate/10, change_rate*2, 20)
dlist = numpy.linspace(change_rate/2,change_rate*2,4)
blist = num_meanings*dlist

sd_max_lh_tree = max_lh.sd_max_lh_free_parameters(topology, blist, dlist, max_tree_height, age_constraints, age_resolution, sd_data)

print("multistate max lh tree")
print(multistate_max_lh_tree)

print("sd max likelihood parameters and tree")
print(sd_max_lh_tree)