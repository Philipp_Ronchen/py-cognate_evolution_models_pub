import gmpy2
from gmpy2 import mpfr
import math

gmpy2.get_context().precision=200
a = mpfr('1.2')
b = mpfr('1.2')
print(type(a+b))
print(a+b)
print(type(a+b))
c = gmpy2.exp(a+b)
print(c)
print(type(c))
d = math.exp(a+b)
print(d)
print(type(d))


print(mpfr(float('1.2')))
print(mpfr('1.1999999999999999555910790149937',250))

print(gmpy2.sqrt(5))
gmpy2.get_context().precision=100
print(gmpy2.sqrt(5))
gmpy2.get_context().precision+=20
print(gmpy2.sqrt(5))

c = mpfr("0.001")
d = 0.3542453464564*gmpy2.exp(-74*c)
print(d)
print(type(d))

a = 2.0000000000000003
new_a = a
for i in range(200000):
    new_a += a
    #print(new_a)
print(new_a)

a = mpfr("2.0000000000000003")
new_a = mpfr("2.0000000000000003")
for i in range(200000):
    new_a += a
    #print(new_a)
print(new_a)