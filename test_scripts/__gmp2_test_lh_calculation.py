import gmpy2
from gmpy2 import mpfr
from small_tools import io_tools
from trees.non_binary_test_trees import nb75l_tree as tree
from generative_models import generate_multistate as gms
from lh_calculation import lh_multistate_recursive as lh_rec, gmp2_lh_multistate_recursive as g_lh_rec
import time

gmpy2.get_context().precision=200

meanings = {"m0", "m1", "m2", "m3", "m4"}
change_rates = {m:0.1 for m in meanings}
mpfr_change_rates = {m:mpfr("0.1") for m in meanings}
cog_data = gms.generate_outcome(tree, meanings, change_rates)
print(tree[3])
cog_data = {m:{node:cog_data[m][node] for node in tree[3]} for m in cog_data.keys()}
io_tools.print_cog_data(cog_data)

start_time = time.time()
lhs_1 = lh_rec.compute_lh(tree, change_rates, cog_data)
print("--- %s seconds ---" % (time.time() - start_time))
print("Recursive method:")
io_tools.print_lhs(lhs_1[0], lhs_1[1])
start_time = time.time()
lhs_2 = g_lh_rec.compute_lh(tree, mpfr_change_rates, cog_data)
print("--- %s seconds ---" % (time.time() - start_time))
print("Recursive method, mpfr:")
print(lhs_2)
io_tools.print_lhs(lhs_2[0], lhs_2[1])


io_tools.draw_tree(tree)
