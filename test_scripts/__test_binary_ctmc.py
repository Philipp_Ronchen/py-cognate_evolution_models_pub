from trees import binary_test_trees as btt
import copy
from data_transform import multistate_data_transf as mdt
from data_transform import multistate_data_to_sd as mdsd
from trees import binary_test_trees as btt
from generative_models import generate_multistate as gm
from lh_calculation import lh_binary_ctmc as lhbmc
from generative_models import generate_binary_ctmc
from small_tools import io_tools as io



topology = btt.b4l_tree_1
#age_constraints = {"a":[0,0], "b":[0,0], "c":[0,0],"d":[0,0],"e":[200,800],"f":[700,1300],"g":[1200,1800]}
#max_tree_height = 1800
#age_resolution = 100
root = topology[2]
leaves = topology[3]

tree = copy.deepcopy(topology)
tree[1] = {("e","a"):500,("e","b"):500,("f","e"):500,("f","c"):1000, ("g","d"):1500, ("g","f"):500}

num_meanings = 2
meanings = {"m" + str(i) for i in range(num_meanings)}
change_rate = 0.001
change_rates = {m:change_rate for m in meanings}
cog_data = gm.generate_outcome(tree, change_rate, num_meanings)
cog_data = mdt.restrict_data_to_nodes(cog_data, leaves)
print("generated cognate data")
print(cog_data)

binary_data = mdsd.multistate_to_binary(cog_data, leaves)
print("binary data")
print(binary_data)

#data = mdt.package_data(change_rates, cog_data, tree)
b = 0.001
d = 0.004
num_parallel_values = lhbmc.get_num_parallel_values(binary_data)
root_freqs = lhbmc.estimate_root_freqs_from_data(num_parallel_values, binary_data)

#print(lhbmc.calculate_parallel_data_log_lh(tree, b, d, root_freqs, binary_data))
print(lhbmc.calculate_lh(tree, b, d, [0.9,0.1], {"a":1, "b":0, "c":0, "d":0}))

print(generate_binary_ctmc.get_n_trait_data_sets(leaves,3))
print(generate_binary_ctmc.calculate_possible_data_lh_sum(tree,b,d,[0.7,0.3],3))