from trees import binary_test_trees as btt
import copy
from generative_models import generate_sd as gsd
from data_transform import sd_data_transf as sddt
from branch_length_tree_operations import max_lh as max_lh
from lh_calculation import lh_stochastic_dollo as lhsd
from lh_calculation import lh_binary_ctmc as lhb
from collections import Counter
from operator import mul
from functools import reduce
import numpy
import math
import time


start_time = time.time()
num_meanings = 5

#file = open("results_" + str(outcome_number) + ".txt", "a")

topology = btt.b4l_tree_1
age_constraints = {"a": [0, 0], "b": [0, 0], "c": [0, 0], "d": [0, 0], "e": [0, 2000], "f": [0, 2000], "g": [0, 2000]}
max_tree_height = 2000
age_resolution = 200
root = topology[2]
leaves = topology[3]

tree = copy.deepcopy(topology)
tree[1] = {("e", "a"): 600, ("e", "b"): 600, ("f", "e"): 400, ("f", "c"): 1000, ("g", "d"): 1400, ("g", "f"): 400}

base_rate = 0.001
true_d = base_rate
true_b = num_meanings * base_rate

##### data generation
sd_trait_data = gsd.generate_outcome(tree, true_b, true_d)
sd_leaf_trait_data = {node:sd_trait_data[node] for node in leaves}
sd_data = sddt.sd_trait_to_patterns(sd_leaf_trait_data)
num_traits = len(sd_data)
binary_data = sddt.sd_trait_to_binary(sd_leaf_trait_data)

print("sd trait data")
print(sd_trait_data)
print("sd leaf trait data")
print(sd_leaf_trait_data)

print("sd pattern data")
print(sd_data)


print("binary data")
print(binary_data)

print("number of traits in outcome")
print(num_traits)

# ##################### SD and BCTMC parameters ######################################################################

sd_dlist = numpy.linspace(base_rate / 10, base_rate * 2, 11)
sd_blist = numpy.linspace(num_meanings * base_rate / 10, num_meanings * base_rate * 2, 11)
ctmc_dlist = numpy.linspace(base_rate / 10, base_rate * 2, 11)
ctmc_blist = copy.deepcopy(ctmc_dlist)

flist = [(f, 1 - f) for f in numpy.linspace(0, 1, 11)]


########### computing true tree SD lh

true_tree_lh = lhsd.compute_lh(tree, true_b, true_d, sd_data, as_log_lh=True)
print("true tree lh")
print(true_tree_lh)


test_b = base_rate
test_d = base_rate
test_f = (0.8, 0.2)
test_ctmc_lh = lhb.calculate_parallel_data_log_lh(tree, test_b, test_d, test_f, binary_data, adjust_for_0_data=True)
print("test ctmc lh")
print(test_ctmc_lh)
test_ctmc_lh_non_adjusted = lhb.calculate_parallel_data_log_lh(tree, test_b, test_d, test_f, binary_data, adjust_for_0_data=False)
print("test ctmc lh - nonadjusted")
print(test_ctmc_lh_non_adjusted)


### SD max lh inference
sd_max_lh_tree = max_lh.sd_max_lh_free_parameters(topology, sd_blist, sd_dlist, max_tree_height, age_constraints,
                                                          age_resolution, sd_data)
sd_parameters = sd_max_lh_tree[0]
sd_best_tree = sd_max_lh_tree[1]
sd_ages = sd_best_tree[1]
sd_lh =  sd_best_tree[2]
print("sd max lh parameters")
print(sd_parameters)
print("sd max lh ages")
print(sd_ages)
print("sd max lh lh")
print(sd_lh)

ctmc_max_lh_tree = max_lh.binary_ctmc_max_lh_free_parameters(topology, ctmc_blist, ctmc_dlist, flist, max_tree_height,
                                                             age_constraints, age_resolution, binary_data,
                                                             adjust_for_0_data=False)

ctmc_parameters = ctmc_max_lh_tree[0]
ctmc_best_tree = ctmc_max_lh_tree[1]
ctmc_ages = ctmc_best_tree[1]
ctmc_lh = ctmc_best_tree[2]
print("ctmc max lh parameters")
print(ctmc_parameters)
print("ctmc max lh ages")
print(ctmc_ages)
print("ctmc max lh lh")
print(ctmc_lh)

print("WIT READJUSTMENT")
ctmc_max_lh_tree = max_lh.binary_ctmc_max_lh_free_parameters(topology, ctmc_blist, ctmc_dlist, flist, max_tree_height,
                                                             age_constraints, age_resolution, binary_data,
                                                             adjust_for_0_data=True)

ctmc_parameters = ctmc_max_lh_tree[0]
ctmc_best_tree = ctmc_max_lh_tree[1]
ctmc_ages = ctmc_best_tree[1]
ctmc_lh = ctmc_best_tree[2]
print("ctmc max lh parameters")
print(ctmc_parameters)
print("ctmc max lh ages")
print(ctmc_ages)
print("ctmc max lh lh")
print(ctmc_lh)

end_time = time.time()
print("elapsed time")
print(end_time - start_time)

