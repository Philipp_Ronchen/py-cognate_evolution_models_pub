from trees import binary_test_trees as btt
from generative_models import loan_tools
from generative_models import generate_multistate_w_loans as gml
from small_tools import io_tools as io
from trees import tree_points as tpo
from trees import tree_tools as tt
import copy

topology = btt.b4l_tree_1
tree = copy.deepcopy(topology)
tree[1] = {("e","a"):500,("e","b"):500,("f","e"):500,("f","c"):1000, ("g","d"):1500, ("g","f"):500}
edges = tree[1]


print(tpo.get_edge_position_from_leaf_path_position(tree, "b", 1000))
point = (("e","a"), 200)
print(tpo.get_root_distance_from_edge_position(tree, point))
print(tpo.get_parallel_points(tree, point))

from_point_0 = (("g","d"), 1000)
to_point_0 = (("f","c"), 500)
loan_0 = (from_point_0, to_point_0)

from_point_1 = (("g","d"), 1100)
to_point_1 = (("f","c"), 600)
loan_1 = (from_point_1, to_point_1)

from_point_2 = (("e","a"), 100)
to_point_2 = (("g","d"), 1100)
loan_2 = (from_point_2, to_point_2)


loan_intensity = 0.0003
change_rate = 0.001

loan_list = [loan_0, loan_1, loan_2] #, (from_point_2, to_point_2)]
loan_list = loan_tools.generate_loans(tree, loan_intensity)
print("generated loans")
print(loan_list)

loan_tree = loan_tools.change_topology_by_loan_list(tree, loan_list)


for i in range(10):
    outcome = gml.generate_one_meaning_given_loans(tree, 0.001, loan_list)
    print("sample outcome")
    print(outcome)
#loan_tree, new_edge_points_output] = loan_tools.change_topology_by_loan(tree, loan, other_loans)
print(loan_tree)

print("generating an outcome directly")
print(gml.generate_one_meaning_outcome(tree, change_rate, loan_intensity))

io.draw_tree(loan_tree)

io.draw_tree(tree)