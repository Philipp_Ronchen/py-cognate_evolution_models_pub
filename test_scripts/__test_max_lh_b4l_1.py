from generative_models import generate_multistate as gm
from branch_length_tree_operations import max_lh
from branch_length_tree_operations import build_trees_and_accum as bta
from branch_length_tree_operations import accum_results_transf as art
from branch_length_tree_operations import accum_functions as af
from data_transform import multistate_data_transf as mdt
from trees import binary_test_trees as btt
import numpy
import copy
from lh_calculation import lh_binary_ctmc as lhb

topology = btt.b4l_tree_1
age_constraints = {"a":[0,0], "b":[0,0], "c":[0,0],"d":[0,0],"e":[0,100],"f":[1000,1000],"g":[1500,1500]}
max_tree_height = 1800
age_resolution = 20
root = topology[2]
leaves = topology[3]

tree = copy.deepcopy(topology)
tree[1] = {("e","a"):500,("e","b"):500,("f","e"):500,("f","c"):1000, ("g","d"):1500, ("g","f"):500}

num_meanings = 2
meanings = {"m" + str(i) for i in range(num_meanings)}
change_rate = 0.001
change_rates = {m:change_rate for m in meanings}
cog_data = gm.generate_outcome(tree, meanings, change_rates)
cog_data = mdt.restrict_data_to_nodes(cog_data, leaves)
print("generated cognate data")
print(cog_data)

data = mdt.package_data(change_rates, cog_data, tree)
sd_data = mdt.multistate_to_patterns(cog_data)
binary_data = mdt.multistate_to_binary(cog_data, leaves)
#print(sd_data)
print(binary_data)

# #################### MULTISTATE ####################################################

multistate_accum_trees = bta.build_trees_and_accumulate(tree, max_tree_height, age_constraints, age_resolution, data,
                                                    af.multistate_leaf_value_function, af.multistate_accum_func)
multistate_value_trees = art.multistate_accum_to_log_lh_trees(multistate_accum_trees, root)


multistate_max_lh_tree = max_lh.get_max_value_tree(multistate_value_trees)

print("multistate max lh tree")
print(multistate_max_lh_tree)


# ##################### SD ######################################################################

#dlist = numpy.linspace(change_rate/10, change_rate*2, 20)
dlist = numpy.linspace(change_rate,change_rate,1)
blist = num_meanings*dlist

sd_max_lh_tree = max_lh.sd_max_lh_free_parameters(topology, blist, dlist, max_tree_height, age_constraints, age_resolution, sd_data)

print("sd max likelihood parameters and tree")
print(sd_max_lh_tree)

# ##################### BINARY CTMC ######################################################################

num_parallel_values = lhb.get_num_parallel_values(binary_data)
root_freqs = lhb.estimate_root_freqs_from_data(num_parallel_values,binary_data)
print("estimated frequencies")
print(root_freqs)
d = change_rate
b = d * root_freqs[1] / root_freqs[0]
dlist = numpy.linspace(d, d*20, 20)
blist = numpy.linspace(b, b*20, 20)
print("b and d as estimated from root_freqs")
print((b,d))
ctmc_data = [b,d,binary_data]
binary_ctmc_accum_trees = bta.build_trees_and_accumulate(tree, max_tree_height, age_constraints, age_resolution, ctmc_data,
                                                    af.binary_ctmc_leaf_value_function, af.binary_ctmc_accum_func)
binary_ctmc_log_lh_trees = art.binary_ctmc_accum_to_log_lh_trees(binary_ctmc_accum_trees, root, root_freqs)

print("binary ctmc max likelihood parameters and trees")
binary_ctmc_max_lh_tree = max_lh.binary_ctmc_max_lh_free_parameters(topology, blist, dlist, max_tree_height,
                                                                    age_constraints, age_resolution, binary_data,
                                                                    root_freqs)
print(binary_ctmc_max_lh_tree)

print("binary ctmc log lh trees")
print(binary_ctmc_log_lh_trees)

print("binary ctmc log lh of true tree")
print(lhb.calculate_parallel_data_log_lh(tree, b, d, root_freqs, binary_data))

