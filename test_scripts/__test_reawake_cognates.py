from trees import binary_test_trees as btt
from generative_models import generate_multistate_w_reawake as gmr
import copy
from small_tools import io_tools as io

topology = btt.b4l_tree_1
tree = copy.deepcopy(topology)
tree[1] = {("e","a"):500,("e","b"):500,("f","e"):500,("f","c"):1000, ("g","d"):1500, ("g","f"):500}
edges = tree[1]

change_rate = 0.001
reawake_rate = 0.001
cog_data = gmr.generate_one_meaning_outcome(tree, change_rate, reawake_rate)
print(cog_data)

io.draw_tree(tree)