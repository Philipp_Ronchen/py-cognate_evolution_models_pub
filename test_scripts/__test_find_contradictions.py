from lh_calculation import multistate_fix_data_contradictions as mfc
from trees import non_binary_test_trees as nbtt
from small_tools import io_tools as io

tree = nbtt.nb7l_tree_1

set_data = {"a":{1}, "b":{2}, "c":{2,3}, "d":{4}, "e":{4,5}, "f":{}, "g":{7}, "j":{1}}

extended_data = mfc.extend_set_data(tree, set_data)
print(extended_data)

io.draw_tree(tree)


#### multiple data_test
one_meaning_data = {"a":{1}, "b":{2}, "c":{2,3}, "d":{4}, "e":{4,5}, "f":{1}, "g":{7}, "j":{1}}