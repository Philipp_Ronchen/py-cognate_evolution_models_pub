from trees import binary_test_trees as btt
from generative_models import generate_sd as gsd
from data_transform import sd_data_transf as sdt
from small_tools import io_tools as io
from lh_calculation import lh_stochastic_dollo as lhsd
import copy

topology = btt.b4l_tree_1
leaves = topology[3]

tree = copy.deepcopy(topology)
tree[1] = {("e", "a"): 600, ("e", "b"): 600, ("f", "e"): 400, ("f", "c"): 1000, ("g", "d"): 1400, ("g", "f"): 400}

num_meanings = 10
d = 0.001
b = num_meanings * d
trait_data = gsd.generate_outcome(tree, b, d)
leaf_trait_data = {node:trait_data[node] for node in leaves}
pattern_data = sdt.sd_trait_to_patterns(leaf_trait_data)
binary_data = sdt.sd_trait_to_binary(leaf_trait_data)
print(leaf_trait_data)
print(pattern_data)
print(binary_data)



io.draw_tree(tree)