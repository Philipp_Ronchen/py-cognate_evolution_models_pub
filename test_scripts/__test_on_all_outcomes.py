from trees import binary_test_trees as btt
import copy
from generative_models import generate_multistate as gm
from generative_models import generate_sd as gsd
from data_transform import multistate_data_transf as mdt
from data_transform import sd_data_transf as sddt
from data_transform import generate_data_combinations as gdc
from branch_length_tree_operations import accum_functions as af
from branch_length_tree_operations import build_trees_and_accum as bta
from branch_length_tree_operations import accum_results_transf as art
from branch_length_tree_operations import max_lh as max_lh
from lh_calculation import lh_stochastic_dollo as lhsd
from lh_calculation import lh_binary_ctmc as lhb
from small_tools import dict_tools
import math
import numpy
import time
import random as rd

topology = btt.b3l_tree
root = topology[2]
leaves = topology[3]
age_constraints = {"a": [0, 0], "b": [0, 0], "c": [0, 0], "d": [0, 2000], "e": [0,2000]}
max_tree_height = 2000
age_resolution = 200

tree = copy.deepcopy(topology)
tree[1] = {("d", "a"): 600, ("d", "b"): 600, ("e", "c"): 1200, ("e", "d"): 600}

max_num_traits = 5
base_rate = 0.001
num_meanings = 3

sd_d = base_rate
sd_b = base_rate * num_meanings
ctmc_b = base_rate
ctmc_d = base_rate
f = [0.7, 0.3]

all_outcomes = gsd.get_possible_data_sets(tree, max_num_traits)
sd_sum = 0
ctmc_total_sum = 0
ctmc_sums = dict()
for i in range(max_num_traits + 1):
    ctmc_sums[i] = 0

for outcome in all_outcomes:
    binary_data = sddt.sd_patterns_to_binary(outcome, leaves)
    sd_log_lh = lhsd.compute_lh(tree, sd_b, sd_d, outcome, True)
    sd_lh = math.exp(sd_log_lh)
    ctmc_log_lh = lhb.calculate_parallel_data_log_lh(tree, ctmc_b, ctmc_d, f, binary_data, adjust_for_0_data=True)
    ctmc_lh = math.exp(ctmc_log_lh)
    if sd_lh > ctmc_lh:
        print(outcome)
        print(binary_data)
        print("sd lh")
        print(sd_lh)
        print("ctmc lh")
        print(ctmc_lh)

        sd_sum += sd_lh

        num_traits = len(outcome)
        ctmc_sums[num_traits] += ctmc_lh
        ctmc_total_sum += ctmc_lh
        print("")

print("sd sum")
print(sd_sum)
print("ctmc sums")
print(ctmc_sums)
print("ctmc total sum")
print(ctmc_total_sum)