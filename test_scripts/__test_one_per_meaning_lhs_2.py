# calculating the probablities that the sd and the binary ctmc model generat outcomes in which all leaf languages have
# one form per meaning, for different parameters

from trees import binary_test_trees as btt
import copy
from data_transform import multistate_data_transf as mdt
from data_transform import generate_data_combinations as gdc
from lh_calculation import lh_stochastic_dollo as lhsd
from lh_calculation import lh_binary_ctmc as lhb
from small_tools import dict_tools
import math
import numpy
import time

topology = btt.b4l_tree_1
age_constraints = {"a": [0, 0], "b": [0, 0], "c": [0, 0], "d": [0, 0], "e": [0, 2000], "f": [0, 2000], "g": [0, 2000]}
max_tree_height = 2000
age_resolution = 200
root = topology[2]
leaves = topology[3]

tree = copy.deepcopy(topology)
tree[1] = {("e", "a"): 600, ("e", "b"): 600, ("f", "e"): 400, ("f", "c"): 1000, ("g", "d"): 1400, ("g", "f"): 400}

num_meanings = 3
base_rate = 0.001

sd_b = num_meanings * base_rate
sd_d = base_rate
ctmc_b = base_rate
ctmc_d = base_rate
ctmc_f = [0.7, 0.3]



all_one_meaning_outcomes = gdc.get_all_multistate_data_sets(leaves)
meanings = {"m" + str(i) for i in range(num_meanings)}
possible_meaning_outcomes = {m:all_one_meaning_outcomes for m in meanings}
all_outcomes = dict_tools.dict_product(possible_meaning_outcomes)
sd_lh_sum = 0
ctmc_lh_sum = 0
for outcome in all_outcomes:
    print("outcome")
    print(outcome)
    sd_data = mdt.multistate_to_patterns(outcome)
    print(sd_data)
    binary_data = mdt.multistate_to_binary(outcome, leaves)
    print(binary_data)
    sd_log_lh = lhsd.compute_lh(tree, sd_b, sd_d, sd_data, True)
    ctmc_log_lh = lhb.calculate_parallel_data_log_lh(tree, ctmc_b, ctmc_d, ctmc_f, binary_data)
    print("log lhs sd and ctmc")
    print(sd_log_lh)
    print(ctmc_log_lh)
    sd_lh = math.exp(sd_log_lh)
    ctmc_lh = math.exp(ctmc_log_lh)
    print("lhs sd and ctmc")
    print(sd_lh)
    print(ctmc_lh)
    sd_lh_sum += sd_lh
    ctmc_lh_sum += ctmc_lh
print("sd lh sum")
print(sd_lh_sum)
print("ctmc lh sum")
print(ctmc_lh_sum)