from trees import binary_test_trees as btt
import copy
from small_tools import io_tools as io
from generative_models import generate_multistate as gm
from data_transform import multistate_data_transf as mdt
from branch_length_tree_operations import accum_functions as af
from branch_length_tree_operations import build_trees_and_accum as bta
from branch_length_tree_operations import accum_results_transf as art
from branch_length_tree_operations import max_lh as max_lh

topology = btt.b2l_tree
tree = copy.deepcopy(topology)
tree[1] = {("c","a"):250,("c","b"):250}
edges = tree[1]
root = tree[2]
leaves = tree[3]

age_constraints = {"a":[0,0], "b":[0,0], "c":[50,500]}
max_tree_height = 500
age_resolution = 10

num_meanings = 500
meanings = {"m" + str(i) for i in range(num_meanings)}
change_rate = 0.001
change_rates = {m:change_rate for m in meanings}
cog_data = gm.generate_outcome(tree, meanings, change_rates)
cog_data = mdt.restrict_data_to_nodes(cog_data, leaves)
print("generated cognate data")
print(cog_data)

data = mdt.package_data(change_rates, cog_data, tree)
sdm_data = mdt.multistate_to_sdm(change_rates, cog_data)
sdm_accum_trees = bta.build_trees_and_accumulate(tree, max_tree_height, age_constraints, age_resolution,
                                                         sdm_data, af.sdm_leaf_value_function,
                                                         af.sdm_accum_func)
sdm_value_trees = art.accum_to_value_trees(sdm_accum_trees, root)
sdm_intensity_trees = art.sdm_get_trees_with_intensities(tree, sdm_value_trees, sdm_data)
sdm_meaning_log_lh_trees = art.sdm_get_meaning_log_lh_trees(sdm_intensity_trees)

sdm_log_lh_trees = art.sdm_get_log_lh_trees(sdm_meaning_log_lh_trees)

sdm_max_lh_tree = max_lh.get_max_value_tree(sdm_log_lh_trees)

print("sdm max lh tree")
print(sdm_max_lh_tree)

print("sdm log lh trees")
print(sdm_log_lh_trees)

#io.draw_tree(tree)
#io.draw_tree(sdm_max_lh_tree)

