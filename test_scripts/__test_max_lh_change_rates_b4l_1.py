from generative_models import generate_multistate as gm
from branch_length_tree_operations import max_lh
from branch_length_tree_operations import build_trees_and_accum as bta
from branch_length_tree_operations import accum_results_transf as art
from branch_length_tree_operations import accum_functions as af
from data_transform import multistate_data_transf as mdt
from data_transform import change_rate_transf as crt
from trees import binary_test_trees as btt
import numpy
import copy
import random
from lh_calculation import lh_multistate_recursive as lhmr
from lh_calculation import lh_binary_ctmc as lhb
from small_tools import io_tools as io

topology = btt.b4l_tree_1
age_constraints = {"a":[0,0], "b":[0,0], "c":[0,0],"d":[0,0],"e":[300,700],"f":[800,1200],"g":[1300,1700]}
max_tree_height = 1800
age_resolution = 100
root = topology[2]
leaves = topology[3]

tree = copy.deepcopy(topology)
tree[1] = {("e","a"):500,("e","b"):500,("f","e"):500,("f","c"):1000, ("g","d"):1500, ("g","f"):500}

num_meanings = 200
meanings = {"m" + str(i) for i in range(num_meanings)}
random_retention_rates = {meaning:random.random() for meaning in meanings}
change_rates = {m:crt.ret_millenium_to_multistate_cr(random_retention_rates[m]) for m in meanings}
print("change rates")
print(change_rates)
cog_data = gm.generate_outcome(tree, meanings, change_rates)
cog_data = mdt.restrict_data_to_nodes(cog_data, leaves)
print("generated cognate data")
print(cog_data)

change_rates_list = list(change_rates.values())
average_change_rate = sum(change_rates_list)/len(change_rates_list)
print("average change rate")
print(average_change_rate)

data = mdt.package_data(change_rates, cog_data, tree)
sd_data = mdt.multistate_to_patterns(cog_data)
binary_data = mdt.multistate_to_binary(cog_data, leaves)
#print(sd_data)
#print(binary_data)

# TRUE TREE MULTISTATE LHS
lhs = lhmr.compute_lh(tree, change_rates, cog_data)
print("multistate lhs of true tree")
print(lhs)

# # TRUE TREE MULTISTATE LHS WITH MODIFIED COGNATE DATA
# modified_cog_data = {m:{node:cog_data[m][node] for node in cog_data[m].keys() if not (node == "a" or node == "d")} for m in meanings}
# print("modifed cog data")
# print(modified_cog_data)
# lhs = lhmr.compute_lh(tree, change_rates, modified_cog_data)
# print("multistate lhs of true tree with modified cognate data")
# print(lhs)

# MULTISTATE

multistate_accum_trees = bta.build_trees_and_accumulate(tree, max_tree_height, age_constraints, age_resolution, data,
                                                    af.multistate_leaf_value_function, af.multistate_accum_func)
multistate_value_trees = art.accum_to_value_trees(multistate_accum_trees, root)
print("multistate value trees")
print(multistate_value_trees)
multistate_log_lh_trees = art.multistate_accum_to_log_lh_trees(multistate_accum_trees, root)
print("multistate value trees")
print(multistate_log_lh_trees)

multistate_max_lh_tree = max_lh.get_max_value_tree(multistate_log_lh_trees)

print("multistate max lh tree")
print(multistate_max_lh_tree)

# same computation for average change rate
dict_of_average_change_rate = {m:average_change_rate for m in meanings}
data_w_average_cr = mdt.package_data(dict_of_average_change_rate, cog_data, tree)

multistate_accum_trees = bta.build_trees_and_accumulate(tree, max_tree_height, age_constraints, age_resolution, data_w_average_cr,
                                                    af.multistate_leaf_value_function, af.multistate_accum_func)
multistate_value_trees = art.multistate_accum_to_log_lh_trees(multistate_accum_trees, root)

multistate_max_lh_tree = max_lh.get_max_value_tree(multistate_value_trees)

print("multistate max lh tree if average change rate is used")
print(multistate_max_lh_tree)


#  MULTISTATE WITH THROW OUTS
# num_throw_outs = 2
# data_w_throw_outs = mdt.package_data_w_throw_outs(change_rates, cog_data, num_throw_outs, tree)
# print("data with throw outs")
# print(data_w_throw_outs)
# multistate_accum_trees = bta.build_trees_and_accumulate(tree, max_tree_height, age_constraints, age_resolution,
#                                                         data_w_throw_outs,
#                                                         af.multistate_w_throw_outs_leaf_value_function,
#                                                         af.multistate_w_throw_outs_accum_func)
# print("throw out trees")
# multistate_value_trees = art.accum_to_value_trees(multistate_accum_trees, root)
# print(multistate_value_trees)
# multistate_trees_w_best_throw_outs = art.multistate_get_best_throw_out_meaning_lh_trees(multistate_value_trees)
# print("best throw out trees")
# print(multistate_trees_w_best_throw_outs)
#
# # CALCULATE ALL LIKELIHOODS FOR THROW OUT DATA
# for key in data_w_throw_outs:
#     print("meaning and throw out nodes")
#     print(key)
#     current_change_rate = data_w_throw_outs[key][0]
#     current_cog_data = data_w_throw_outs[key][2]
#     lhs = lhmr.compute_lh(tree, {"m":current_change_rate}, {"m":current_cog_data})
#     print(lhs)

# MEANINGWISE STOCHASTIC DOLLO
sdm_data = mdt.multistate_to_sdm(change_rates, cog_data)

print("sdm data")
print(sdm_data)

sdm_accum_trees = bta.build_trees_and_accumulate(tree, max_tree_height, age_constraints, age_resolution,
                                                         sdm_data, af.sdm_leaf_value_function,
                                                         af.sdm_accum_func)
#print("sdm accum trees")
#print(sdm_accum_trees)
sdm_value_trees = art.accum_to_value_trees(sdm_accum_trees, root)
#print("sdm value trees")
#print(sdm_value_trees)
sdm_intensity_trees = art.sdm_get_trees_with_intensities(tree, sdm_value_trees, sdm_data)
sdm_meaning_log_lh_trees = art.sdm_get_meaning_log_lh_trees(sdm_intensity_trees)

print("sdm meaning log lh trees")
print(sdm_meaning_log_lh_trees)

sdm_log_lh_trees = art.sdm_get_log_lh_trees(sdm_meaning_log_lh_trees)
#sdm_log_lh_trees_directly = art.sdm_accum_to_log_lh_trees(tree, sdm_accum_trees, sdm_data)

print("sdm log lh trees")
print(sdm_log_lh_trees)

sdm_max_lh_tree = max_lh.get_max_value_tree(sdm_log_lh_trees)
print("sdm max lh tree")
print(sdm_max_lh_tree)

dlist = [average_change_rate]
blist = [num_meanings*average_change_rate]

sd_max_lh_tree = max_lh.sd_max_lh_free_parameters(topology, blist, dlist, max_tree_height, age_constraints, age_resolution, sd_data)

print("sd max likelihood parameters and tree")
print(sd_max_lh_tree)


#
# # BINARY CTMC
#
# num_parallel_values = lhb.get_num_parallel_values(binary_data)
# root_freqs = lhb.estimate_root_freqs_from_data(num_parallel_values,binary_data)
# print("estimated frequencies")
# print(root_freqs)
# d = average_change_rate
# b = d * root_freqs[1] / root_freqs[0]
# dlist = numpy.linspace(d, d, 1)
# blist = numpy.linspace(b, b, 1)
# print("b and d as estimated from root_freqs")
# print((b,d))
# ctmc_data = [b,d,binary_data]
# binary_ctmc_accum_trees = bta.build_trees_and_accumulate(tree, max_tree_height, age_constraints, age_resolution, ctmc_data,
#                                                     af.binary_ctmc_leaf_value_function, af.binary_ctmc_accum_func)
# binary_ctmc_log_lh_trees = art.binary_ctmc_accum_to_log_lh_trees(binary_ctmc_accum_trees, root, root_freqs)
#
# print("binary ctmc log lh trees, given estimated b and d")
# print(binary_ctmc_log_lh_trees)
#
# print("binary ctmc max likelihood tree given estimated b and d")
# binary_ctmc_max_lh_tree_given_paramters = max_lh.get_max_value_tree(binary_ctmc_log_lh_trees)
# print(binary_ctmc_max_lh_tree_given_paramters)
#
# print("binary ctmc max likelihood parameters and trees")
# binary_ctmc_max_lh_tree = max_lh.binary_ctmc_max_lh_free_parameters(topology, blist, dlist, max_tree_height,
#                                                                     age_constraints, age_resolution, binary_data,
#                                                                     root_freqs)
# print(binary_ctmc_max_lh_tree)
#
#
#
# print("binary ctmc log lh of true tree")
# print(lhb.calculate_parallel_data_log_lh(tree, b, d, root_freqs, binary_data))
#
#
io.draw_tree(tree)
#
