def sd_trait_to_patterns(sd_trait_data):
    data_nodes = sd_trait_data.keys()
    patterns = list()
    traits = {trait for node in data_nodes for trait in sd_trait_data[node]}
    for trait in traits:
        pattern = frozenset({node for node in data_nodes if trait in sd_trait_data[node]})
        patterns.append(pattern)
    return patterns


def sd_trait_to_binary(sd_trait_data):
    data_nodes = sd_trait_data.keys()
    trait_set = {trait for node in data_nodes for trait in sd_trait_data[node]}
    trait_list = list(trait_set)
    num_traits = len(trait_list)
    binary_data = dict()
    for node in data_nodes:
        node_binary_data = [0] * num_traits
        for i in range(num_traits):
            if trait_list[i] in sd_trait_data[node]:
                node_binary_data[i] = 1
        binary_data[node] = node_binary_data
    return binary_data


def sd_patterns_to_binary(patterns, data_nodes):
    num_traits = len(patterns)
    binary_data = dict()
    for node in data_nodes:
        node_binary_data = [0] * num_traits
        for i in range(num_traits):
            if node in patterns[i]:
                node_binary_data[i] = 1
        binary_data[node] = node_binary_data
    return binary_data
