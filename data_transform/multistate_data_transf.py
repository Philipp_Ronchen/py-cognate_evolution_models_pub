from lh_calculation import lh_multistate_recursive as lhmr
import itertools
import more_itertools
import copy

# given change_rates, cog_data and tree_topology constructs a data format suitable for use of the multistate
# accumulation function. The data format has the {meaning:[change_rate, is_extendable, extended_data]...}
def package_data(change_rates, cog_data, tree_topology):
    meanings = cog_data.keys()
    packaged_data = dict()
    for meaning in meanings:
        change_rate = change_rates[meaning]
        meaning_cog_data = cog_data[meaning]
        [is_extendable, extended_data] = lhmr.extend_data(tree_topology, meaning_cog_data)
        packaged_data[meaning] = (change_rate, is_extendable, extended_data)
    return packaged_data


# given change_rates, multiple_cog_data and tree_topology constructs a data format suitable for use of the multistate
# accumulation function. The data format has the {meaning:[data_0, data_1,...]...} where the data_i have the form
# [change_rate, is_extendable, extended_data]. When using this package function, the change_rates differ only by meaning
# and are the same for all data_sets for a given meaning
def package_multiple_data(change_rates, multiple_cog_data, tree_topology):
    meanings = multiple_cog_data.keys()
    packaged_data = dict()
    for meaning in meanings:
        change_rate = change_rates[meaning]
        meaning_cog_data_sets = multiple_cog_data[meaning]
        num_meaning_data_sets = len(meaning_cog_data_sets)
        packaged_data[meaning] = [None] * num_meaning_data_sets
        for i in range(num_meaning_data_sets):
            [is_extendable, extended_data] = lhmr.extend_data(tree_topology, meaning_cog_data_sets[i])
            packaged_data[meaning][i] = (change_rate, is_extendable, extended_data)
    return packaged_data


# given change_rates, cog_data and tree_topology constructs and a number of forms that may be thrown out when
# calculating the multistate likelihood, constructs a data format suitable for use of the multistate
# accumulation function. In the simple package_data functions.
# The output has the structure {meaning:[change_rate, throw_out_data]...}, where throw_out_data has the structure
# {throw_out_combination:[is_extendable, extended_data]...}
def package_data_w_throw_outs(change_rates, cog_data, num_throw_outs, tree_topology):
    meanings = cog_data.keys()
    packaged_data_w_throw_outs = dict()
    for meaning in meanings:
        meaning_cog_data = cog_data[meaning]
        change_rate = change_rates[meaning]
        data_nodes = cog_data[meaning].keys()
        throw_outs_combs = itertools.combinations(data_nodes, num_throw_outs)
        throw_out_data = dict()
        packaged_data_w_throw_outs[meaning] = [change_rate, throw_out_data]
        for throw_out_comb in throw_outs_combs:
            throw_out_comb = frozenset(throw_out_comb)
            data_nodes_left = {node for node in data_nodes if node not in throw_out_comb}
            reduced_cog_data = {node:meaning_cog_data[node] for node in data_nodes_left}
            [is_extendable, extended_data] = lhmr.extend_data(tree_topology, reduced_cog_data)
            throw_out_data[throw_out_comb] = [is_extendable, extended_data]
    return packaged_data_w_throw_outs


# restricts data of the format {meaning:{node:data_point...}..} to a given node_set
def restrict_data_to_nodes(cog_data, node_set):
    restricted_data = dict()
    for meaning in cog_data.keys():
        this_meaning_data = cog_data[meaning]
        restricted_data[meaning] = {node:this_meaning_data[node] for node in node_set}
    return restricted_data


# generates all possible combinations of multistate data on a given set of nodes, without taking into account the
# multistate model and the tree topology.
def get_data_combinations(nodes):
    partitions = more_itertools.set_partitions(nodes)
    data_combinations = list()
    for partition in partitions:
        new_comb = dict()
        num_parts = len(partition)
        for i in range(num_parts):
            part = partition[i]
            for node in part:
                new_comb[node] = i
        data_combinations.append(new_comb)
    return data_combinations


# trasforms a multistate data set into a list of patterns
def multistate_to_patterns(multistate_data):
    meanings = multistate_data.keys()
    patterns = list()
    for meaning in meanings:
        meaning_data = multistate_data[meaning]
        new_patterns = multistate_one_meaning_to_patterns(meaning_data)
        patterns += new_patterns
    return patterns


# transforms a one meaning multistate data set into a list of patterns
def multistate_one_meaning_to_patterns(one_meaning_data):
    covered_nodes = one_meaning_data.keys()
    patterns = list()
    cognate_codes = set(one_meaning_data.values())
    for value in cognate_codes:
        pattern = {node for node in covered_nodes if one_meaning_data[node] == value}
        pattern = frozenset(pattern)
        patterns.append(pattern)
    return patterns


# transforms a setwise multistate data set (divided by meaning) to a meaningwise pattern data set
def multistate_setwise_to_mw_patterns(setwise_multistate_data):
    meanings = sorted(setwise_multistate_data.keys())
    mw_patterns = dict()
    for m in meanings:
        one_meaning_multistate_data = setwise_multistate_data[m]
        covered_nodes = one_meaning_multistate_data.keys()
        patterns = list()
        cognate_codes = set().union(*list(one_meaning_multistate_data.values()))
        for value in cognate_codes:
            pattern = {node for node in covered_nodes if value in one_meaning_multistate_data[node]}
            pattern = frozenset(pattern)
            patterns.append(pattern)
        mw_patterns[m] = patterns
    return mw_patterns


# transforms a multistate data set into the binary format
def multistate_to_binary(multistate_data, data_nodes):
    meanings = sorted(multistate_data.keys())
    one_meaning_transformations = dict()
    for meaning in meanings:
        one_meaning_transformations[meaning] = multistate_one_meaning_to_binary(multistate_data[meaning])
    binary_data = {node:list() for node in data_nodes}
    for meaning in meanings:
        for node in data_nodes:
            binary_data[node] += one_meaning_transformations[meaning][node]
    return binary_data


# transforms a one meaning multistate data set into the binary format
def multistate_one_meaning_to_binary(one_meaning_data):
    data_nodes = one_meaning_data.keys()
    data_values = {one_meaning_data[node] for node in one_meaning_data.keys()}
    num_data_values = len(data_values)
    binary_data = dict()
    for node in data_nodes:
        binary_data[node] = [0]*num_data_values
    data_values_list = list(data_values)
    for node in data_nodes:
        for i in range(num_data_values):
            if one_meaning_data[node] == data_values_list[i]:
                binary_data[node][i] = 1
    return binary_data


# transforms multistate data and change rates to meaningwise stochastic dollo data, where the parameters of the sdm
# model are chosen to match the multistate model as closely as possible
def multistate_to_sdm(change_rates, cog_data):
    meanings = cog_data.keys()
    sdm_data = dict()
    for meaning in meanings:
        death_rate = change_rates[meaning]
        birth_rate = 1 * death_rate
        patterns = multistate_one_meaning_to_patterns(cog_data[meaning])
        sdm_data[meaning] = [birth_rate, death_rate, patterns]
    return sdm_data


# transforms setwise multistate data and change rates to meaningwise stochastic dollo data, where the parameters
# of the sdm model are chosen to match the multistate model as closely as possible. That means the death rate
# is the same as the change rate of the multistate model, while the birth rate for every meaning is chosen to obtain
# the same equilibrium number of traits as is seen in the data
def multistate_setwise_to_sdm_estimate_eq(change_rates, setwise_multistate_data):
    meanings = setwise_multistate_data.keys()
    mw_patterns = multistate_setwise_to_mw_patterns(setwise_multistate_data)
    death_rates = copy.deepcopy(change_rates)
    estimated_traits_per_lang = dict()
    for m in meanings:
        traits_per_lang = list(map(len, list(setwise_multistate_data[m].values())))
        estimated_traits_per_lang[m] = sum(traits_per_lang) / len(traits_per_lang)
    sdm_data = dict()
    for m in meanings:
        death_rate = death_rates[m]
        birth_rate = estimated_traits_per_lang[m] * death_rate
        patterns = mw_patterns[m]
        sdm_data[m] = [birth_rate, death_rate, patterns]
    return sdm_data

