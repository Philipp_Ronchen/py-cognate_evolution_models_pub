import copy


# given a set of natural numbers used_values, returns the minimum natural number not in used_values
def get_min_unused_value(used_values):
    max_used_value = max(used_values)
    min_unused_value = min({value for value in range(max_used_value + 2) if value not in used_values})
    return min_unused_value


def replace_data_value(new_value, old_value, nodes_affected, one_meaning_data):
    new_data = copy.deepcopy(one_meaning_data)
    for node in nodes_affected:
        if node in one_meaning_data and one_meaning_data[node] == old_value:
            new_data[node] = new_value
    return new_data


# changes cognate class codes such that all numbers in [0, ..., num_cog_classes - 1] are used
def simplify_data(one_meaning_data):
    used_values = set(one_meaning_data.values())
    new_data = dict()
    current_code = 0
    for value in used_values:
        for node in one_meaning_data.keys():
            if one_meaning_data[node] == value:
                new_data[node] = current_code
        current_code += 1
    return new_data