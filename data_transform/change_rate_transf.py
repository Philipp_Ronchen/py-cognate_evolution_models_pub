from small_tools import math_tools


# transform the fraction of words that are retained per millenium to the change rate used in the multistate model
def ret_millenium_to_multistate_cr(ret_millenium):
    change_rate = -math_tools.inf_log(ret_millenium) / 1000
    return change_rate