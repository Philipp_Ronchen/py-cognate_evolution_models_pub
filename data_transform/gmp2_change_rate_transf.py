from small_tools import gmp2_math_tools as math_tools
from gmpy2 import mpfr

# gmp2 version of change_rate_transf


# transform the fraction of words that are retained per millenium to the change rate used in the multistate model
# pass ret_millenium as strings or integer
def ret_millenium_to_multistate_cr(ret_millenium):
    change_rate = (-1) * math_tools.inf_log(mpfr(ret_millenium)) / 1000
    return change_rate