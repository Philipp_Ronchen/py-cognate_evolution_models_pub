import more_itertools


def get_all_multistate_data_sets(nodes):
    data_sets = list()
    partitions = more_itertools.set_partitions(nodes)
    for partition in partitions:
        partition_cog_data = dict()
        num_classes = len(partition)
        for i in range(num_classes):
            for node in partition[i]:
                partition_cog_data[node] = i
        data_sets.append(partition_cog_data)
    return data_sets